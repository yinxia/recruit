FROM anapsix/alpine-java:8

EXPOSE 8093

COPY target/*.jar /app/app.jar

ENV JAVA_OPTS=""

ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -jar /app/app.jar" ]
