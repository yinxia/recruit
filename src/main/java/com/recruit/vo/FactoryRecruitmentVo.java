package com.recruit.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.recruit.entity.FactoryRecruitmentType;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class FactoryRecruitmentVo {

    private String id;

    private Long createBy;

    private String helpWanted;

    private String settlementType;

    private String startSalary;

    private String endSalary;

    private String workingPlace;

    private String salary;

    //private int FRType;

    //private int category;

    //private String name;

    //private int num;

    private String authenticationType;

    private String distance;

    private String city;

    private String province;

    private String area;

    private String results;

    private String cause;

    private String userVxNumber;

    private String userPhone;

    private String vxNumber;

    private String phone;

    private String contact;

    private String recruitmentInstructions;

    private String releaseType;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createDate;

    private String jobs;

    private String recruitType;

    private String otherContent;

    private List<FactoryRecruitmentType> voList;
}
