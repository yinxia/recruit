package com.recruit.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class PostArticleVo {

    private String id;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createDate;

    private String content;

    private String photoId;

    private Long userId;

    private String results;

    private String cause;

    private String otherContent;

    private String userVxNumber;

    private String userPhone;
}
