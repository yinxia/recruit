package com.recruit.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class RentHouseVo {

    private String id;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createDate;

    private String title;

    private String houseArea;

    private String doorModel;

    private String toward;

    private String floor;

    private int elevator;

    private String houseProperty;

    private String decorateSituation;

    private String money;

    private String phone;

    private String vxNumber;

    private String contact;

    private String houseAddress;

    private String province;

    private String city;

    private String area;

    private String houseInstructions;

    private Long userId;

    private String photoId;

    private String results;

    private String cause;

    private String otherContent;

    private String userVxNumber;

    private String userPhone;

    private String authenticationType;

}
