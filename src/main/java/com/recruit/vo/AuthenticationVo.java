package com.recruit.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class AuthenticationVo {

    private String id;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createDate;

    private String enterpriseName;

    private String businessLicense;

    private String businessScenarios;

    private String industry;

    private String introduction;

    private String address;

    private String province;

    private String city;

    private String area;

    private String contact;

    private String phone;

    private String vxNumber;

    private String type;

    private String idCard;

    private String idPhoto;

    private String idAddress;

    private String results;

    private String cause;

    private String otherContent;

    private Long userId;

    private String userVxNumber;

    private String userPhone;

}
