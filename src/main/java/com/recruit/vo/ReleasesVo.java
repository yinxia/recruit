package com.recruit.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class ReleasesVo {

    private String id;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createDate;

    private String enterpriseName;

    private String category;

    private String other;

    private String specifications;

    private String phone;

    private String vxNumber;

    private String contact;

    private String detailedAddress;

    private String companiesIntroduce;

    private String companiesPhotoId;

    private int typeList;

    private String results;

    private String cause;

    private String otherContent;

    private String userVxNumber;

    private String userPhone;

    private String money;

    private int secondTransferType;

}
