package com.recruit.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class JobInformationVo {


    private String id;

    private Long createBy;

    private String startSalary;

    private String endSalary;

    private String salary;

    private String userVxNumber;

    private String userPhone;

    private String vxNumber;

    private String phone;

    private String nickname;

    private int sex;

    private int age;

    private String job;

    private String industry;

    private String functions;

    private String nativeProvince;

    private String nativeCity;

    private String nativeProvinceCity;

    private String workExperience;

    private String province;

    private String city;

    private String area;

    private String village;

    private String expectPlace;

    private String results;

    private String cause;

    private String otherContent;

    private int jobStatus;

    private int workTime;

    private String selfAssessment;

    private String email;

    private int releaseType;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createDate;

    private Long infoUploadFileId;

}
