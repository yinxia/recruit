package com.recruit.controller;


import com.recruit.beans.ApiResult;
import com.recruit.entity.Members;
import com.recruit.services.MembersService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/members")
@Log4j
public class MembersController {

    @Autowired
    private MembersService service;

    @PostMapping("/saveOrUpdate")
    public ApiResult<?> saveOrUpdate(Members members){
        try{
            return ApiResult.prepare(service.saveOrUpdate(members));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

}
