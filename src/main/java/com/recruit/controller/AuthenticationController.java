package com.recruit.controller;


import com.recruit.beans.ApiResult;
import com.recruit.entity.Authentication;
import com.recruit.pager.Pager;
import com.recruit.services.AuthenticationService;
import com.recruit.utils.SensitivewordFilter;
import com.recruit.vo.AuthenticationVo;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;

@RestController
@RequestMapping("/api/authentication")
@Log4j
public class AuthenticationController {

    @Autowired
    private AuthenticationService service;


    @PostMapping("/saveOrUpdate")
    public ApiResult<?> saveOrUpdate(Authentication authentication){
        try{
            SensitivewordFilter sensitivewordFilter = new SensitivewordFilter();
            if(sensitivewordFilter.getSensitiveWord(authentication.getIndustry(),1)){
                return ApiResult.prepareError("内容含有敏感词!");
            }

            return ApiResult.prepare(service.saveOrUpdate(authentication));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    @GetMapping("/details")
    public ApiResult<?> details(Long id){
        try{
            return ApiResult.prepare(service.details(id));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }


    @GetMapping("/getPagerAllList")
    public ApiResult<?> getPagerAllList(Pager<Authentication> pager, AuthenticationVo vo, Date startTime, Date endTime) {
        try {
            return ApiResult.prepare(service.pagerList(pager,vo,startTime,endTime));
        } catch (Exception e) {
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    @DeleteMapping("/dels")
    public ApiResult<?> dels(Long[] id){
        try {
            if (id == null) {
                return ApiResult.prepareError("删除失败。");
            }
            Arrays.stream(id).forEach(i -> {
                service.delete(i);
            });
            return ApiResult.prepare("删除成功。");
        }catch (Exception e){
            log.error("错误日志： " +  e);
            return ApiResult.prepareError("删除失败。");
        }
    }


    @PostMapping("/updateResults")
    public ApiResult<?> updateResults(Authentication authentication){
        try{
            return ApiResult.prepare(service.updateResults(authentication));
        }catch (Exception e){
            log.error("错误日志： " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    @PostMapping("/batchResults")
    public ApiResult<?> batchResults(Long ids[]){
        try {
            service.batchResults(ids);
            return ApiResult.prepare("批量成功。");
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("批量失败。");
        }
    }

    @GetMapping("/findCountResults")
    public ApiResult<?> findCountResults(){
        try{
            return ApiResult.prepare(service.findCountResults());
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    @GetMapping("/findCountResultsAndType")
    public ApiResult<?> findCountResultsAndType(String type){
        try{
            return ApiResult.prepare(service.findCountResultsAndType(type));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }





}
