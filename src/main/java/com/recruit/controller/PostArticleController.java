package com.recruit.controller;


import com.recruit.beans.ApiResult;
import com.recruit.entity.PostArticle;
import com.recruit.pager.Pager;
import com.recruit.services.PostArticleService;
import com.recruit.utils.SensitivewordFilter;
import com.recruit.vo.PostArticleVo;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/api/postArticle")
@Log4j
public class PostArticleController {

    @Autowired
    private PostArticleService service;

    @PostMapping("/saveOrUpdate")
    public ApiResult<?> saveOrUpdate(PostArticle postArticle){
        try {
            SensitivewordFilter sensitivewordFilter = new SensitivewordFilter();
            if (null != postArticle.getContent() && sensitivewordFilter.getSensitiveWord(postArticle.getContent(), 1)) {
                return ApiResult.prepareError("内容含有敏感词!");
            }

            return ApiResult.prepare(service.saveOrUpdate(postArticle));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("添加失败。");
        }
    }

    @GetMapping("/pagerList")
    public ApiResult<?> pagerList(Pager<PostArticle> pager, PostArticleVo vo, Date startTime, Date endTime){
        try {
            return ApiResult.prepare(service.pagerList(pager, vo, startTime, endTime));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }


    @PostMapping("/updateResults")
    public ApiResult<?> updateResults(PostArticle postArticle){
        try {
            return ApiResult.prepare(service.updateResults(postArticle));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("改变状态失败。");
        }
    }

    @PostMapping("/batchResults")
    public ApiResult<?> batchResults(Long ids[]){
        try {
            service.batchResults(ids);
            return ApiResult.prepare("批量成功。");
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("批量失败。");
        }
    }



    @GetMapping("/findCountNum")
    public ApiResult<?> findCountNum(){
        try {
            return ApiResult.prepare(service.findCountNum());
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }


}
