package com.recruit.controller;

import com.recruit.beans.ApiResult;
import com.recruit.entity.Members;
import com.recruit.entity.Order;
import com.recruit.entity.User;
import com.recruit.pager.Pager;
import com.recruit.services.OrderService;
import com.recruit.services.UserService;
import com.recruit.vo.OrderVo;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;


@RestController
@RequestMapping("/api/order")
@Log4j
public class OrderController {

    @Autowired
    private OrderService service;

    @Autowired
    private UserService userService;

    @PostMapping("/saveOrUpdate")
    public ApiResult<?> saveOrUpdate(Order order,Members members){
        try{
            return ApiResult.prepare(service.saveOrUpdate(order,members));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    @PostMapping("/membersAuthorization")
    public ApiResult<?> membersAuthorization(Order order,String phone){
        try {
            return ApiResult.prepare(service.membersAuthorization(order,phone));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("授权失败。");
        }
    }

    @PostMapping("/undo")
    public ApiResult<?> undo(Long id){
        try {
            Order order = service.findOne(id);
            if (null != order) {
                order.setDays(0);
                SimpleDateFormat df = new SimpleDateFormat("yyyy");
                order.setStartTime(df.parse("0000"));
                order.setEndTime(df.parse("0000"));
                order.setUndoTime(new Date());
                order.setPayOrNot("2");
            }
            User user = userService.findOne(order.getUserId());
            if(null != user && !user.getUserType().equals("2")){
                user.setUserType("2");
                userService.update(user);
            }
            return ApiResult.prepare(service.update(order));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("撤销失败。");
        }
    }

    @GetMapping("/pagerList")
    public ApiResult<?> pagerList(Pager<Order> pager, OrderVo vo, Date startTime, Date endTime){
        try {
            return ApiResult.prepare(service.pagerList(pager,vo,startTime,endTime));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    @GetMapping("/sumMoney")
    public ApiResult<?> sumMoney(OrderVo vo, Date startTime, Date endTime){
        try {
            return ApiResult.prepare(service.sumMoney(vo,startTime,endTime));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }



}
