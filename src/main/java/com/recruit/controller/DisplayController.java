package com.recruit.controller;


import com.recruit.beans.ApiResult;
import com.recruit.entity.Display;
import com.recruit.services.DisplayService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/display")
@Log4j
public class DisplayController {

    @Autowired
    private DisplayService service;

    @PostMapping("/saveOrUpdate")
    public ApiResult<?> saveOrUpdate(Display display){

        try{
            return ApiResult.prepare(service.saveOrUpdate(display));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }
}
