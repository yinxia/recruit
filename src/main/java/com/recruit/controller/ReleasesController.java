package com.recruit.controller;


import com.recruit.beans.ApiResult;
import com.recruit.entity.Releases;
import com.recruit.pager.Pager;
import com.recruit.services.ReleasesService;
import com.recruit.utils.SensitivewordFilter;
import com.recruit.vo.ReleasesVo;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/api/releases")
@Log4j
public class ReleasesController {

    @Autowired
    private ReleasesService service;


    @PostMapping("/saveOrUpdate")
    public ApiResult<?> saveOrUpdate(Releases releases){
        try {
            SensitivewordFilter sensitivewordFilter = new SensitivewordFilter();

            if (null != releases.getEnterpriseName() && sensitivewordFilter.getSensitiveWord(releases.getEnterpriseName(), 1)) {
                return ApiResult.prepareError("内容含有敏感词!");
            }

            if (null != releases.getSpecifications() && sensitivewordFilter.getSensitiveWord(releases.getSpecifications(), 1)) {
                return ApiResult.prepareError("内容含有敏感词!");
            }

            if (null != releases.getCompaniesIntroduce() && sensitivewordFilter.getSensitiveWord(releases.getCompaniesIntroduce(), 1)) {
                return ApiResult.prepareError("内容含有敏感词!");
            }
            return ApiResult.prepare(service.saveOrUpdate(releases));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("添加失败。");
        }
    }


    @GetMapping("/pagerList")
    public ApiResult<?> pagerList(Pager<Releases> pager, ReleasesVo vo, Date startTime, Date endTime){
        try {
            return ApiResult.prepare(service.pagerList(pager, vo, startTime, endTime));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    @PostMapping("/updateResults")
    public ApiResult<?> updateResults(Releases releases){
        try {
            return ApiResult.prepare(service.updateResults(releases));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("改变状态失败。");
        }
    }


    @PostMapping("/batchResults")
    public ApiResult<?> batchResults(Long ids[]){
        try {
            service.batchResults(ids);
            return ApiResult.prepare("批量成功。");
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("批量失败。");
        }
    }

    @GetMapping("/findCountNum")
    public ApiResult<?> findCountNum(int typeList){
        try {
            return ApiResult.prepare(service.findCountNum(typeList));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    @GetMapping("/findCountNumAndType")
    public ApiResult<?> findCountNumAndType(int typeList,int secondTransferType){
        try {
            return ApiResult.prepare(service.findCountNumAndType(typeList,secondTransferType));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }


}
