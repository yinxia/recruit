package com.recruit.controller;


import com.recruit.beans.ApiResult;
import com.recruit.entity.FactoryRecruitment;
import com.recruit.entity.FactoryRecruitmentType;
import com.recruit.pager.Pager;
import com.recruit.services.FactoryRecruitmentService;
import com.recruit.services.FactoryRecruitmentTypeService;
import com.recruit.utils.SensitivewordFilter;
import com.recruit.vo.FactoryRecruitmentVo;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/factoryRecruitment")
@Log4j
public class FactoryRecruitmentController {

    @Autowired
    private FactoryRecruitmentService service;

    @Autowired
    private FactoryRecruitmentTypeService factoryRecruitmentTypeService;

    @PostMapping("/saveOrUpdate")
    public ApiResult<?> saveOrUpdate(String json,FactoryRecruitment factoryRecruitment){
        try{
            SensitivewordFilter sensitivewordFilter = new SensitivewordFilter();
            if(sensitivewordFilter.getSensitiveWord(factoryRecruitment.getRecruitmentInstructions(),1)){
                return ApiResult.prepareError("内容含有敏感词!");
            }

            return ApiResult.prepare(service.saveOrUpdate(json,factoryRecruitment));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    @GetMapping("/getPagerAllList")
    public ApiResult<?> getPagerAllList(Pager<FactoryRecruitment> pager, FactoryRecruitmentVo vo, String [] types){
        try {
            return ApiResult.prepare(service.getPagerAllList2(pager, vo, types));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    @GetMapping("/details")
    public ApiResult<?> details(Long id){

        try {
            return ApiResult.prepare(service.details(id));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    @DeleteMapping("/del")
    public ApiResult<?> del(Long id){
        try {
            FactoryRecruitment factoryRecruitment = service.findOne(id);
            if (null != factoryRecruitment) {
                List<FactoryRecruitmentType> typeList = factoryRecruitmentTypeService.
                        findByFactoryRecruitmentId(factoryRecruitment.getId());
                if (null != typeList && typeList.size() > 0) {
                    typeList.stream().forEach(k -> {
                        factoryRecruitmentTypeService.delete(k.getId());
                    });
                }
            }
            service.delete(id);
            return ApiResult.prepare("删除成功。");
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("删除失败。");
        }
    }

    @PostMapping("/updateResults")
    public ApiResult<?> updateResults(FactoryRecruitment factoryRecruitment){
        try {
            return ApiResult.prepare(service.updateResults(factoryRecruitment));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("改变状态失败。");
        }
    }

    @GetMapping("/pagerList")
    public ApiResult<?> pagerList(Pager<FactoryRecruitment> pager, FactoryRecruitmentVo vo, Date startTime, Date endTime){
        try {
            return ApiResult.prepare(service.pagerList(pager, vo, startTime, endTime));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }


    @PostMapping("/batchResults")
    public ApiResult<?> batchResults(Long ids[]){
        try {
            service.batchResults(ids);
            return ApiResult.prepare("批量成功。");
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("批量失败。");
        }
    }

    @GetMapping("/findCountResults")
    public ApiResult<?> findCountResults(){
        try {
            return ApiResult.prepare(service.findCountResults());
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    @GetMapping("/findCountRecruitType")
    public ApiResult<?> findCountRecruitType(String type){
        try {
            return ApiResult.prepare(service.findCountRecruitType(type));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }





}
