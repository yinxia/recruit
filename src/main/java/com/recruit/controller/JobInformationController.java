package com.recruit.controller;


import com.recruit.beans.ApiResult;
import com.recruit.entity.JobInformation;
import com.recruit.pager.Pager;
import com.recruit.services.JobInformationService;
import com.recruit.vo.JobInformationVo;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/api/jobInformation")
@Log4j
public class JobInformationController {


    @Autowired
    private JobInformationService service;

    @PostMapping("/saveOrUpdate")
    public ApiResult<?> saveOrUpdate(String json){
        try{
            boolean saveOrupdate = service.saveOrUpdate(json);
            if(!saveOrupdate){
                return ApiResult.prepareError("内容含有敏感词!");
            }
            return ApiResult.prepare("添加成功。");
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }


    @GetMapping("/pagerList")
    public ApiResult<?> pagerList(Pager<JobInformation> pager, JobInformationVo vo, Date startTime, Date endTime){
        try {
            return ApiResult.prepare(service.pagerList(pager, vo, startTime, endTime));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }


    @GetMapping("/details")
    public ApiResult<?> details(Long id){
        try {
            return ApiResult.prepare(service.details(id));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }


    @PostMapping("/updateResults")
    public ApiResult<?> updateResults(JobInformation jobInformation){
        try {
            return ApiResult.prepare(service.updateResults(jobInformation));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("改变状态失败。");
        }
    }


    @PostMapping("/batchResults")
    public ApiResult<?> batchResults(Long ids[]){
        try {
            service.batchResults(ids);
            return ApiResult.prepare("批量成功。");
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("批量失败。");
        }
    }


    @GetMapping("/findCountNum")
    public ApiResult<?> findCountNum(){
        try {
            return ApiResult.prepare(service.findCountNum());
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }

    @GetMapping("/findCountNumAndReleaseType")
    public ApiResult<?> findCountNumAndReleaseType(int type){
        try {
            return ApiResult.prepare(service.findCountNumAndReleaseType(type));
        }catch (Exception e){
            log.error("错误日志：  " + e);
            return ApiResult.prepareError("系统维护。");
        }
    }


}
