package com.recruit.entity;


import com.recruit.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

@Entity
@Table(name = "re_renthouse")
@Data
@EqualsAndHashCode
public class RentHouse extends BaseModel {

    @Column(name = "title", columnDefinition = "VARCHAR(200)   COMMENT '发布标题'")
    private String title;

    @Column(name = "house_area", columnDefinition = "VARCHAR(56)   COMMENT '房子面积'")
    private String houseArea;

    @Column(name = "door_model", columnDefinition = "VARCHAR(56)   COMMENT '户型'")
    private String doorModel;

    @Column(name = "toward", columnDefinition = "VARCHAR(56)   COMMENT '朝向'")
    private String toward;

    @Column(name = "floor", columnDefinition = "VARCHAR(56)   COMMENT '楼层'")
    private String floor;

    @Column(name = "elevator", columnDefinition = "int  DEFAULT 0   COMMENT '电梯 1.有 2.无'")
    private int elevator;

    @Column(name = "house_property", columnDefinition = "VARCHAR(56)   COMMENT '房子性质'")
    private String houseProperty;

    @Column(name = "decorate_situation", columnDefinition = "VARCHAR(56)   COMMENT '装修情况'")
    private String decorateSituation;

    @Column(name = "money", columnDefinition = "VARCHAR(56) COMMENT '租金'")
    private String money;

    @Column(name = "phone", columnDefinition = "VARCHAR(56) COMMENT '联系电话'")
    private String phone;

    @Column(name = "vx_number", columnDefinition = "VARCHAR(56) COMMENT '微信号码'")
    private String vxNumber;

    @Column(name = "contact", columnDefinition = "VARCHAR(56) COMMENT '联系人'")
    private String contact;

    @Column(name = "house_address", columnDefinition = "VARCHAR(255)   COMMENT '房子地址'")
    private String houseAddress;

    @Column(name = "province", columnDefinition = "VARCHAR(50) COMMENT '省'")
    private String province;

    @Column(name = "city", columnDefinition = "VARCHAR(50) COMMENT '市'")
    private String city;

    @Column(name = "area", columnDefinition = "VARCHAR(50) COMMENT '区'")
    private String area;

    @Column(name = "house_instructions", columnDefinition = "VARCHAR(255)   COMMENT '房子说明'")
    private String houseInstructions;

    @Column(name = "user_id", columnDefinition = "bigint(20) COMMENT '用户id'")
    private Long userId;

    @Column(name ="photo_id",columnDefinition = "VARCHAR(255) COMMENT '图片id'")
    private String photoId;

    @Column(name ="results",columnDefinition = "VARCHAR(56) COMMENT '审核结果 1.上线中 2.审核不通过 3.审核中 4.下线中'")
    private String results;

    @Column(name ="cause",columnDefinition = "VARCHAR(255) COMMENT '原因'")
    private String cause;

    @Column(name ="other_content",columnDefinition = "VARCHAR(255) COMMENT '其他内容'")
    private String otherContent;


    @Transient
    private List<InfoUploadFile> photoIds;

}
