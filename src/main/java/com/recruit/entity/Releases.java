package com.recruit.entity;

import com.recruit.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

@Entity
@Table(name = "re_releases")
@Data
@EqualsAndHashCode
public class Releases extends BaseModel {

    @Column(name = "enterprise_name", columnDefinition = "VARCHAR(200)   COMMENT '企业名称'")
    private String enterpriseName;

    @Column(name = "category", columnDefinition = "VARCHAR(56)   COMMENT '需求类别'")
    private String category;

    @Column(name = "other", columnDefinition = "VARCHAR(56)   COMMENT '其他'")
    private String other;

    @Column(name = "specifications", columnDefinition = "VARCHAR(255)   COMMENT '需求说明'")
    private String specifications;

    @Column(name = "phone", columnDefinition = "VARCHAR(56) COMMENT '联系电话'")
    private String phone;

    @Column(name = "vx_number", columnDefinition = "VARCHAR(56) COMMENT '微信号码'")
    private String vxNumber;

    @Column(name = "contact", columnDefinition = "VARCHAR(56) COMMENT '联系人'")
    private String contact;

    @Column(name = "detailed_address", columnDefinition = "VARCHAR(255)   COMMENT '详细地址'")
    private String detailedAddress;

    @Column(name = "companies_introduce", columnDefinition = "VARCHAR(255)   COMMENT '企业介绍'")
    private String companiesIntroduce;

    @Column(name ="companies_photo_id",columnDefinition = "VARCHAR(255) COMMENT '企业场景'")
    private String companiesPhotoId;

    @Column(name = "type_list", columnDefinition = "int  DEFAULT 0   COMMENT '具体列表'")
    private int typeList;

    @Column(name ="results",columnDefinition = "VARCHAR(56) COMMENT '审核结果 1.上线中 2.审核不通过 3.审核中 4.下线中'")
    private String results;

    @Column(name ="cause",columnDefinition = "VARCHAR(255) COMMENT '原因'")
    private String cause;

    @Column(name ="other_content",columnDefinition = "VARCHAR(255) COMMENT '其他内容'")
    private String otherContent;

    @Column(name = "second_transfer_type", columnDefinition = "int  DEFAULT 0   COMMENT '二手转让类型 1.二手转让 2.二手求购'")
    private int secondTransferType;

    @Column(name = "money", columnDefinition = "VARCHAR(56) COMMENT '转让价格'")
    private String money;

    @Column(name = "user_id", columnDefinition = "bigint(20) COMMENT '用户id'")
    private Long userId;


    //企业场景
    @Transient
    private List<InfoUploadFile> companiesPhotoIds;


}
