package com.recruit.entity;

import com.recruit.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="display")
@Data
@EqualsAndHashCode
public class Display extends BaseModel {

    @Column(name = "is_members", columnDefinition = "int  DEFAULT 0   COMMENT '会员功能 1.显示 2.不显示'")
    private int isMembers;

    @Column(name = "is_authentication", columnDefinition = "int  DEFAULT 0   COMMENT '认证功能 1.显示 2.不显示'")
    private int isAuthentication;


}
