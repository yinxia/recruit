package com.recruit.entity;

import com.recruit.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name="re_factory_recruitment_type")
@Data
@EqualsAndHashCode
public class FactoryRecruitmentType extends BaseModel {

    //1.生产类 2.管理类 3.销售类 4.文职类 5.销售类2 6.裁床类  7.尾部  8.后勤类
    @Column(name = "type", columnDefinition = "VARCHAR(56)  COMMENT '类型'")
    private String type;

    @Column(name = "type_name", columnDefinition = "VARCHAR(56)   COMMENT '类型名称'")
    private String typeName;

    //生产类
    //1.整件车位 2.流水组长 3.新手车位 4.整件组长 5.四针六线 6.生产文员 7.冚车 8.中查
    //9.埋夹 10.手工 11.开袋 12.打边 13.车脚 14.三线 15.中烫 16.四线 17.双针 18.五线 19.其他

    //管理类
    //1.外发主管 2.生产主管 3.生产厂长 4.生产经理 5.生产跟单 6.QC跟单 7.其他

    //销售类
    //1.销售业务 2.外贸跟单 3.销售经理 4.业务助理 5.其他

    //文职类
    //1.会计 2.主账会计 3.会计文员 4.会计主管 5.采购 6.文员 7.前台 8.其他

    //销售类2
    //1.销售经理 2.销售精英 3.销售助理 4.电话销售 5.业务员 6.业务助理 7.其他

    //裁床类
    //1.杂工 2.拉布 3.裁床主管 4.电剪 5.验片员 6.裁床助理 7.其他

    //尾部
    //1.专机 2.包装 3.大烫 4.查货 5.仓管 6.剪线 7.返修 8.收发 9.尾部主管 10.机修 11.其他

    //后勤类
    //1.配货员 2.司机 3.食堂阿姨 4.杂工 5.厨师 6.保安 7.清洁工 8.保安队长 9.其他

    @Column(name = "name", columnDefinition = "VARCHAR(56)   COMMENT '类别名称'")
    private String name;

    @Column(name = "content", columnDefinition = "VARCHAR(255)   COMMENT '其他描述'")
    private String content;

    //1.工厂发布 2.设计版师发布 3.临工快招 4.更多招聘 5.档口招聘 6.电商&主播招聘 7.合作加工
    //8.二手转让 9.布行辅料 10.绣印洗水
    @Column(name = "release_type", columnDefinition = "VARCHAR(56)   COMMENT '发布类型'")
    private String releaseType;

    @Column(name = "factoryrecruitment_id", columnDefinition = "bigint(20)   COMMENT '发布id'")
    private Long factoryRecruitmentId;

    @Column(name = "industry", columnDefinition = "VARCHAR(255)   COMMENT '行业'")
    private String industry;

    @Column(name = "functions", columnDefinition = "VARCHAR(56)   COMMENT '职能'")
    private String functions;

    @Column(name = "job", columnDefinition = "VARCHAR(56)   COMMENT '岗位'")
    private String job;

    @Column(name = "number", columnDefinition = "int  DEFAULT 0 COMMENT '人数'")
    private int number;

}
