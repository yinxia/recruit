package com.recruit.entity;

import com.recruit.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

@Entity
@Table(name="re_postarticle")
@Data
@EqualsAndHashCode
public class PostArticle extends BaseModel {


    @Column(name = "content", columnDefinition = "VARCHAR(255)   COMMENT '贴子内容'")
    private String content;

    @Column(name = "photo_id", columnDefinition = "VARCHAR(255)   COMMENT '贴子图片'")
    private String photoId;

    @Column(name = "user_id", columnDefinition = "bigint(20) COMMENT '用户id'")
    private Long userId;

    @Column(name ="results",columnDefinition = "VARCHAR(56) COMMENT '审核结果 1.上线中 2.审核不通过 3.审核中 4.下线中'")
    private String results;

    @Column(name ="cause",columnDefinition = "VARCHAR(255) COMMENT '原因'")
    private String cause;

    @Column(name ="other_content",columnDefinition = "VARCHAR(255) COMMENT '其他内容'")
    private String otherContent;

    @Transient
    private List<InfoUploadFile> photoIds;
}

