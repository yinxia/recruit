package com.recruit.entity;

import com.recruit.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="members")
@Data
@EqualsAndHashCode
public class Members extends BaseModel {


    @Column(name = "members_type", columnDefinition = "VARCHAR(20) COMMENT '会员类型 1.月 2.年'")
    private String membersType;

    @Column(name = "money1", columnDefinition = "double COMMENT '现价金额'")
    private Double money1;

    @Column(name = "money2", columnDefinition = "double COMMENT '原价金额'")
    private Double money2;

    @Column(name = "instructions", columnDefinition = "VARCHAR(255) COMMENT '会员权益说明'")
    private String instructions;

}
