package com.recruit.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.recruit.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;


@Entity
@Table(name="re_order")
@Data
@EqualsAndHashCode
public class Order extends BaseModel {

    @Column(name = "user_id", columnDefinition = "bigint(20) COMMENT '用户id'")
    private Long userId;

    @Column(name = "members_id", columnDefinition = "bigint(20) COMMENT '会员id'")
    private Long membersId;

    @Column(name = "start_time", columnDefinition = "DATETIME COMMENT '开始时间'")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date startTime;

    @Column(name = "end_time", columnDefinition = "DATETIME COMMENT '结束时间'")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date endTime;

    @Column(name = "pay_or_not", columnDefinition = "VARCHAR(20) COMMENT '是否支付 1.是 2.否'")
    private String payOrNot;

    @Column(name = "undo_time", columnDefinition = "DATETIME COMMENT '撤销时间'")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date undoTime;

    @Column(name = "days", columnDefinition = "int  DEFAULT 0   COMMENT '天数'")
    private int days;


}
