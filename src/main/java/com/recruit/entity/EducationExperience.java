package com.recruit.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.recruit.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name="re_education_experience")
@Data
@EqualsAndHashCode
public class EducationExperience extends BaseModel {

    @Column(name = "start_time",  columnDefinition = "DATETIME COMMENT '开始时间'")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date startTime;

    @Column(name = "end_time",  columnDefinition = "DATETIME COMMENT '结束时间'")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date endTime;

    @Column(name = "school", columnDefinition = "VARCHAR(56)   COMMENT '学校'")
    private String school;

    @Column(name = "education", columnDefinition = "VARCHAR(56)   COMMENT '学历'")
    private String education;

    @Column(name = "full_time", columnDefinition = "int  DEFAULT 0   COMMENT '全日制 1.是  2.否'")
    private int fullTime;

    @Column(name = "professional", columnDefinition = "VARCHAR(56)   COMMENT '专业'")
    private String professional;

    @Column(name = "professional_describe", columnDefinition = "VARCHAR(56)   COMMENT '专业描述'")
    private String professionalDescribe;

    @Column(name = "abroad_experience", columnDefinition = "int  DEFAULT 0   COMMENT '留学经验 1.是 2.否'")
    private int abroadExperience;

    @Column(name = "jobInformation_id", columnDefinition = "bigint(20)   COMMENT '求职信息id'")
    private Long jobInformationId;


}
