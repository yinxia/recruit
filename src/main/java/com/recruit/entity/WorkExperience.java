package com.recruit.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.recruit.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

@Entity
@Table(name="re_work_experience")
@Data
@EqualsAndHashCode
public class WorkExperience  extends BaseModel {

    @Column(name = "start_time",  columnDefinition = "DATETIME COMMENT '开始时间'")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date startTime;

    @Column(name = "end_time",  columnDefinition = "DATETIME COMMENT '结束时间'")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date endTime;

    @Column(name = "company", columnDefinition = "VARCHAR(56)   COMMENT '公司'")
    private String company;

    @Column(name = "industry", columnDefinition = "VARCHAR(56)   COMMENT '行业'")
    private String industry;

    @Column(name = "job", columnDefinition = "VARCHAR(56)   COMMENT '岗位'")
    private String job;

    @Column(name = "company_size", columnDefinition = "VARCHAR(56)   COMMENT '公司规模'")
    private String companySize;

    @Column(name = "company_nature", columnDefinition = "VARCHAR(56)   COMMENT '公司性质'")
    private String companyNature;

    @Column(name = "job_description", columnDefinition = "VARCHAR(255)   COMMENT '工作描述'")
    private String jobDescription;

    @Column(name = "jobInformation_id", columnDefinition = "bigint(20)   COMMENT '求职信息id'")
    private Long jobInformationId;


}
