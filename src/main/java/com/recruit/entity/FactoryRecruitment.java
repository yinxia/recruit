package com.recruit.entity;

import com.recruit.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

@Entity
@Table(name="re_factory_recruitment")
@Data
@EqualsAndHashCode
public class FactoryRecruitment extends BaseModel {


    @Column(name = "settlement_type", columnDefinition = "VARCHAR(56)   COMMENT '结算类型 1.月结 2.日结'")
    private String settlementType;

    @Column(name = "help_wanted", columnDefinition = "VARCHAR(56)   COMMENT '急聘 1.否 2.是'")
    private String helpWanted;

    /*//1.整件车位 2.流水组长 3.新手车位 4.整件组长 5.四针六线 6.生产文员 7.冚车 8.中查
    //9.埋夹 10.手工 11.开袋 12.打边 13.车脚 14.三线 15.中烫 16.四线 17.双针 18.五线 19.其他
    @Column(name = "production", columnDefinition = "VARCHAR(56)   COMMENT '生产类'")
    private String production;

    @Column(name = "production_describe", columnDefinition = "VARCHAR(255)   COMMENT '其他描述'")
    private String productionDescribe;

    //1.外发主管 2.生产主管 3.生产厂长 4.生产经理 5.生产跟单 6.QC跟单 7.其他
    @Column(name = "management", columnDefinition = "VARCHAR(56)   COMMENT '管理类'")
    private String management;

    @Column(name = "management_describe", columnDefinition = "VARCHAR(255)   COMMENT '其他描述'")
    private String managementDescribe;

    //1.销售业务 2.外贸跟单 3.销售经理 4.业务助理 5.其他
    @Column(name = "sales", columnDefinition = "VARCHAR(56)   COMMENT '销售类'")
    private String sales;

    @Column(name = "sales_describe", columnDefinition = "VARCHAR(255)   COMMENT '其他描述'")
    private String salesDescribe;

    //1.会计 2.主账会计 3.会计文员 4.会计主管 5.采购 6.文员 7.前台 8.其他
    @Column(name = "civilian", columnDefinition = "VARCHAR(56)   COMMENT '文职类'")
    private String civilian;

    @Column(name = "civilian_describe", columnDefinition = "VARCHAR(255)   COMMENT '其他描述'")
    private String civilianDescribe;

    //1.销售经理 2.销售精英 3.销售助理 4.电话销售 5.业务员 6.业务助理 7.其他
    @Column(name = "sales2", columnDefinition = "VARCHAR(56)   COMMENT '销售类'")
    private String sales2;

    @Column(name = "sales2_describe", columnDefinition = "VARCHAR(255)   COMMENT '其他描述'")
    private String sales2Describe;

    //1.杂工 2.拉布 3.裁床主管 4.电剪 5.验片员 6.裁床助理 7.其他
    @Column(name = "tailor", columnDefinition = "VARCHAR(56)   COMMENT '裁缝类'")
    private String tailor;

    @Column(name = "tailor_describe", columnDefinition = "VARCHAR(255)   COMMENT '其他描述'")
    private String tailorDescribe;

    //1.专机 2.包装 3.大烫 4.查货 5.仓管 6.剪线 7.返修 8.收发 9.尾部主管 10.机修 11.其他
    @Column(name = "tail", columnDefinition = "VARCHAR(56)   COMMENT '裁缝类'")
    private String tail;

    @Column(name = "tail_describe", columnDefinition = "VARCHAR(255)   COMMENT '其他描述'")
    private String tailDescribe;

    //1.配货员 2.司机 3.食堂阿姨 4.杂工 5.厨师 6.保安 7.清洁工 8.保安队长 9.其他
    @Column(name = "logistics", columnDefinition = "VARCHAR(56)   COMMENT '后勤类'")
    private String logistics;

    @Column(name = "logistics_describe", columnDefinition = "VARCHAR(255)   COMMENT '其他描述'")
    private String logisticsDescribe;*/

    @Column(name = "recruitment_instructions", columnDefinition = "VARCHAR(255)   COMMENT '招聘说明'")
    private String recruitmentInstructions;

    @Column(name = "start_salary", columnDefinition = "VARCHAR(56)   COMMENT '开始薪资'")
    private String startSalary;

    @Column(name = "end_salary", columnDefinition = "VARCHAR(56)   COMMENT '结束薪资'")
    private String endSalary;

    @Column(name = "working_place", columnDefinition = "VARCHAR(200)   COMMENT '工作地点'")
    private String workingPlace;

    @Column(name = "contact", columnDefinition = "VARCHAR(56) COMMENT '联系人'")
    private String contact;

    @Column(name = "phone", columnDefinition = "VARCHAR(56) COMMENT '联系电话'")
    private String phone;

    @Column(name = "vx_number", columnDefinition = "VARCHAR(56) COMMENT '微信号码'")
    private String vxNumber;

    @Column(name = "browse", columnDefinition = "bigint(255) COMMENT '浏览次数'")
    private int browse;

    @Column(name = "distance", columnDefinition = "VARCHAR(56) COMMENT '距离'")
    private String distance;

    @Column(name ="results",columnDefinition = "VARCHAR(56) COMMENT '审核结果 1.上线中 2.审核不通过 3.审核中 4.下线中'")
    private String results;

    @Column(name ="cause",columnDefinition = "VARCHAR(255) COMMENT '原因'")
    private String cause;

    @Column(name ="other_content",columnDefinition = "VARCHAR(255) COMMENT '其他内容'")
    private String otherContent;

    @Column(name ="upload_photo",columnDefinition = "VARCHAR(255) COMMENT '上传图片'")
    private String uploadPhoto;

    @Column(name ="title_name",columnDefinition = "VARCHAR(255) COMMENT '标题名称'")
    private String titleName;

    @Column(name ="recruit_type",columnDefinition = "VARCHAR(20) COMMENT '招聘类型'")
    private String recruitType;

    @Column(name = "user_id", columnDefinition = "bigint(20) COMMENT '用户id'")
    private Long userId;




    @Transient
    private String salary;

    @Transient
    private Authentication authentication;

    @Transient
    private List<FactoryRecruitmentType> factoryRecruitmentTypes;

    //上传图片
    @Transient
    private List<InfoUploadFile> uploadPhotos;




}
