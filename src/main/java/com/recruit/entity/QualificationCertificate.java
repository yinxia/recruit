package com.recruit.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.recruit.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name="re_qualification_certificate")
@Data
@EqualsAndHashCode
public class QualificationCertificate extends BaseModel {

    @Column(name = "obtain_time",  columnDefinition = "DATETIME COMMENT '获得时间'")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date obtainTime;

    @Column(name = "certificate", columnDefinition = "VARCHAR(56)   COMMENT '证书'")
    private String certificate;

    @Column(name = "achievement", columnDefinition = "VARCHAR(56)   COMMENT '成绩'")
    private String achievement;

    @Column(name = "jobInformation_id", columnDefinition = "bigint(20)   COMMENT '求职信息id'")
    private Long jobInformationId;
}
