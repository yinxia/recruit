package com.recruit.entity;


import com.recruit.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

@Entity
@Table(name = "re_job_information")
@Data
@EqualsAndHashCode
public class JobInformation extends BaseModel {

    @Column(name = "nickname", columnDefinition = "VARCHAR(56) COMMENT '姓名'")
    private String nickname;

    @Column(name = "sex", columnDefinition = "int  DEFAULT 0   COMMENT '性别 1.男 2.女'")
    private int sex;

    @Column(name = "age", columnDefinition = "int  DEFAULT 0   COMMENT '年龄'")
    private int age;

    @Column(name = "release_type", columnDefinition = "int  DEFAULT 0   COMMENT '发布类型 1.临工 2.个人'")
    private int releaseType;

    @Column(name = "native_province", columnDefinition = "VARCHAR(50) COMMENT '籍贯-省'")
    private String nativeProvince;

    @Column(name = "native_city", columnDefinition = "VARCHAR(50) COMMENT '籍贯-市'")
    private String nativeCity;

    @Column(name = "start_salary", columnDefinition = "VARCHAR(56)   COMMENT '开始薪资'")
    private String startSalary;

    @Column(name = "end_salary", columnDefinition = "VARCHAR(56)   COMMENT '结束薪资'")
    private String endSalary;

    @Column(name = "industry", columnDefinition = "VARCHAR(255)   COMMENT '行业'")
    private String industry;

    @Column(name = "functions", columnDefinition = "VARCHAR(56)   COMMENT '职能'")
    private String functions;

    @Column(name = "job", columnDefinition = "VARCHAR(56)   COMMENT '岗位'")
    private String job;

    @Column(name = "content", columnDefinition = "VARCHAR(255)   COMMENT '其他岗位'")
    private String content;

    @Column(name = "work_experience", columnDefinition = "VARCHAR(255)   COMMENT '工作经验'")
    private String workExperience;

    @Column(name = "province", columnDefinition = "VARCHAR(50) COMMENT '省'")
    private String province;

    @Column(name = "city", columnDefinition = "VARCHAR(50) COMMENT '市'")
    private String city;

    @Column(name = "area", columnDefinition = "VARCHAR(50) COMMENT '区'")
    private String area;

    @Column(name = "village", columnDefinition = "VARCHAR(56) COMMENT '村'")
    private String village;

    @Column(name = "phone", columnDefinition = "VARCHAR(56) COMMENT '联系电话'")
    private String phone;

    @Column(name = "vx_number", columnDefinition = "VARCHAR(56) COMMENT '微信号码'")
    private String vxNumber;

    @Column(name = "job_status", columnDefinition = "int  DEFAULT 0   COMMENT '求职状态 1.离职 2.在职'")
    private int jobStatus;

    @Column(name = "work_time",  columnDefinition = "int  DEFAULT 0  COMMENT '最快到岗时间 1.随时 2.一周 3.一个月 4.待定'")
    private int workTime;

    @Column(name = "infoUploadFile_id", columnDefinition = "bigint(20)   COMMENT '文件id'")
    private Long infoUploadFileId;

    @Column(name = "self_assessment", columnDefinition = "VARCHAR(255) COMMENT '自我评价'")
    private String selfAssessment;

    @Column(name ="results",columnDefinition = "VARCHAR(56) COMMENT '审核结果 1.上线中 2.审核不通过 3.审核中 4.下线中'")
    private String results;

    @Column(name ="cause",columnDefinition = "VARCHAR(255) COMMENT '原因'")
    private String cause;

    @Column(name ="other_content",columnDefinition = "VARCHAR(255) COMMENT '其他内容'")
    private String otherContent;

    @Column(name ="email",columnDefinition = "VARCHAR(100) COMMENT '邮箱'")
    private String email;

    @Column(name = "user_id", columnDefinition = "bigint(20) COMMENT '用户id'")
    private Long userId;

    @Transient
    private List<WorkExperience> workExperienceList;

    @Transient
    private List<EducationExperience> educationExperienceList;

    @Transient
    private List<QualificationCertificate> qualificationCertificateList;





}
