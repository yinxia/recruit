package com.recruit.entity;

import com.recruit.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

@Entity
@Table(name="re_authentication")
@Data
@EqualsAndHashCode
public class Authentication extends BaseModel {

    @Column(name = "enterprise_name", columnDefinition = "VARCHAR(255)   COMMENT '企业名称/个人姓名'")
    private String enterpriseName;

    @Column(name ="business_license",columnDefinition = "VARCHAR(200) COMMENT '营业执照'")
    private String businessLicense;

    @Column(name ="business_scenarios",columnDefinition = "VARCHAR(2000) COMMENT '企业场景'")
    private String businessScenarios;

    @Column(name ="industry",columnDefinition = "VARCHAR(56) COMMENT '所属行业'")
    private String industry;

    @Column(name ="introduction",columnDefinition = "VARCHAR(255) COMMENT '简介'")
    private String introduction;

    @Column(name ="address",columnDefinition = "VARCHAR(200) COMMENT '详细地址'")
    private String address;

    @Column(name = "province", columnDefinition = "VARCHAR(50) COMMENT '省'")
    private String province;

    @Column(name = "city", columnDefinition = "VARCHAR(50) COMMENT '市'")
    private String city;

    @Column(name = "area", columnDefinition = "VARCHAR(50) COMMENT '区'")
    private String area;

    @Column(name = "contact", columnDefinition = "VARCHAR(56) COMMENT '联系人'")
    private String contact;

    @Column(name = "phone", columnDefinition = "VARCHAR(56) COMMENT '联系电话'")
    private String phone;

    @Column(name = "vx_number", columnDefinition = "VARCHAR(56) COMMENT '微信号码'")
    private String vxNumber;

    @Column(name = "type", columnDefinition = "VARCHAR(56) COMMENT '认证类型 1.企业 2.个人 3.无'")
    private String type;

    @Column(name = "id_card", columnDefinition = "VARCHAR(200) COMMENT '身份证号码'")
    private String idCard;

    @Column(name ="id_photo",columnDefinition = "VARCHAR(255) COMMENT '身份证照片'")
    private String idPhoto;

    @Column(name ="id_address",columnDefinition = "VARCHAR(255) COMMENT '身份证地址'")
    private String idAddress;

    @Column(name ="results",columnDefinition = "VARCHAR(56) COMMENT '审核结果 1.通过 2.不通过 3.无'")
    private String results;

    @Column(name ="cause",columnDefinition = "VARCHAR(255) COMMENT '原因'")
    private String cause;

    @Column(name ="other_content",columnDefinition = "VARCHAR(255) COMMENT '其他内容'")
    private String otherContent;

    @Column(name = "user_id", columnDefinition = "bigint(20) COMMENT '用户id'")
    private Long userId;

    @Transient
    private String userVxNumber;

    @Transient
    private String userPhone;


    //企业场景
    @Transient
    private List<InfoUploadFile> businessScenarios1;

    @Transient
    private List<InfoUploadFile> idPhotos;

    @Transient
    private InfoUploadFile businessLicenses;
}
