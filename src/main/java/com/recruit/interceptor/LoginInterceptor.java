/*
package com.store.interceptor;

import com.store.utils.StringTools;
import com.store.utils.Constant;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

//登录检查拦截器
@Component
@Log4j
public class LoginInterceptor implements HandlerInterceptor {
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		response.setCharacterEncoding("UTF-8");
		//防止乱码，适用于传输JSON数据
		response.setHeader("Content-Type","application/json;charset=UTF-8");
		String uri = request.getRequestURI();
		//log.error("=============   "+request.getHeader(Constant.HEADER_AUTHORIZATION));
		if (request.getMethod().equals(RequestMethod.OPTIONS.name())) {
			response.setStatus(HttpServletResponse.SC_OK);
			setHeader(request,response);
			response.getWriter().write("OPTIONS returns OK");
			//log.error("=== ===="+request.getHeader(Constant.HEADER_AUTHORIZATION));
			return true;
		}
		if (StringUtils.containsAny(uri,"api/user/login", "/pub","interf","/upload" )) {
			return true;
		}

		String header = request.getHeader(Constant.HEADER_AUTHORIZATION);
		if(StringUtils.isEmpty(header)){
			header = request.getParameter("access_token");
		}
		if(StringUtils.isEmpty(header)){
			Map<String, Object> result = new HashMap<>();
			result.put("code", 2);
			result.put("msg", "请传递Token");
			result.put("header","header:" +header);
			result.put("Origin","Origin:" +request.getHeader("Origin"));
			try {
				response.getWriter().write(StringTools.toJSON(result));
			} catch (Exception e) {
				e.printStackTrace();
			}
			return false;
		}
		header = StringTools.trimBearer(header);
		try {
			JWT.parseToken(header);
		} catch (Exception e) {
			response.setStatus(403);
			try {
				Map<String, Object> result = new HashMap<>();
				result.put("code", 2);
				result.put("msg", "登录状态已经过期，请重新登录" + uri );
				response.getWriter().write(StringTools.toJSON(result));
			} catch (Exception e1) {
			}
		}
		// 放行
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

	}

	*/
/**
	 * 为response设置header，实现跨域
	 *//*

	private void setHeader(HttpServletRequest request,HttpServletResponse response){
		//跨域的header设置
		response.setHeader("Access-control-Allow-Origin", request.getHeader("Origin"));
		response.setHeader("Access-Control-Allow-Credentials", "true");
		response.setHeader("Access-Control-Allow-Methods", "GET, HEAD, POST, PUT, PATCH, DELETE, OPTIONS");
		response.setHeader("Access-Control-Max-Age", "86400");
		response.setHeader("Access-Control-Allow-Headers", "Content-Type,authorization");
	}
}
*/
