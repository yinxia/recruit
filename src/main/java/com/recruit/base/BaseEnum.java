package com.recruit.base;

public interface BaseEnum {

    Integer getCode() ;

    String getMessage() ;

}