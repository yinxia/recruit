package com.recruit.repository;

import com.recruit.base.BaseRepository;
import com.recruit.entity.Authentication;
import org.springframework.data.jpa.repository.Query;

public interface AuthenticationRepository extends BaseRepository<Authentication,Long> {


    Authentication findByUserId(Long id);

    @Query("select count(id) from Authentication where results is null  ")
    int findCountResults();

    @Query("select count(id) from Authentication where results is null and type =?1 ")
    int findCountResultsAndType(String type);

}
