package com.recruit.repository;

import com.recruit.base.BaseRepository;
import com.recruit.entity.QualificationCertificate;

import java.util.List;

public interface QualificationCertificateRepository extends BaseRepository<QualificationCertificate,Long> {

    List<QualificationCertificate> findByJobInformationId(Long jobInformationId);
}
