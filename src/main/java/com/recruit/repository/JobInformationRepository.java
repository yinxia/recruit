package com.recruit.repository;

import com.recruit.base.BaseRepository;
import com.recruit.entity.JobInformation;
import org.springframework.data.jpa.repository.Query;

public interface JobInformationRepository extends BaseRepository<JobInformation,Long> {

    @Query("select count(id) from JobInformation where results is null ")
    int findCountNum();

    @Query("select count(id) from JobInformation where results is null and releaseType =?1 ")
    int findCountNumAndReleaseType(int releaseType);
}
