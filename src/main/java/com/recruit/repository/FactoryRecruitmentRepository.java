package com.recruit.repository;

import com.recruit.base.BaseRepository;
import com.recruit.entity.FactoryRecruitment;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;


public interface FactoryRecruitmentRepository extends BaseRepository<FactoryRecruitment,Long> {


    @Modifying
    @Query(" update FactoryRecruitment set browse = ?1 where id = ?2 ")
    int updateBrowse(int browse, Long id);

    @Query("select count(id) from FactoryRecruitment where results is null  ")
    int findCountResults();

    @Query("select count(id) from FactoryRecruitment where results is null and recruitType =?1 ")
    int findCountRecruitType(String type);
}
