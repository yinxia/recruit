package com.recruit.repository;

import com.recruit.entity.User;
import com.recruit.base.BaseRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;

/**
 * @ClassName: StoreUserRepository
 * @Author:
 * @Date: 19-6-27 下午2:46
 * @Desription: ${}
 * @Copy: ${com.bjike}
 **/
public interface UserRepository extends BaseRepository<User,Long> {


    User findByPhone(String phone);

    @Modifying
    @Query(" update User set loginTime = ?1 where phone = ?2 ")
    int updateLoginTime(Date loginTime, String phone);
}
