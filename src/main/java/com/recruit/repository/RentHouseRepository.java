package com.recruit.repository;

import com.recruit.base.BaseRepository;
import com.recruit.entity.RentHouse;
import org.springframework.data.jpa.repository.Query;

public interface RentHouseRepository  extends BaseRepository<RentHouse,Long> {


    @Query("select count(id) from RentHouse where results is null")
    int findCountNum();
}
