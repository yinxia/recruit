package com.recruit.repository;

import com.recruit.base.BaseRepository;
import com.recruit.entity.EducationExperience;

import java.util.List;

public interface EducationExperienceRepository extends BaseRepository<EducationExperience,Long> {


    List<EducationExperience> findByJobInformationId(Long jobInformationId);
}
