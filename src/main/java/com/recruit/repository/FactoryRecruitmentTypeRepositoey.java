package com.recruit.repository;

import com.recruit.base.BaseRepository;
import com.recruit.entity.FactoryRecruitmentType;

import java.util.List;

public interface FactoryRecruitmentTypeRepositoey extends BaseRepository<FactoryRecruitmentType,Long> {


    List<FactoryRecruitmentType> findByFactoryRecruitmentId(Long factoryRecruitmentTypeId);
}
