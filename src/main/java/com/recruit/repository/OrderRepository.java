package com.recruit.repository;

import com.recruit.base.BaseRepository;
import com.recruit.entity.Order;

public interface OrderRepository extends BaseRepository<Order,Long> {

    Order findByUserId(Long userId);
}
