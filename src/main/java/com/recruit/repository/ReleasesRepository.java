package com.recruit.repository;

import com.recruit.base.BaseRepository;
import com.recruit.entity.Releases;
import org.springframework.data.jpa.repository.Query;

public interface ReleasesRepository extends BaseRepository<Releases,Long> {

    @Query("select count(id) from Releases where results is null and typeList =?1")
    int findCountNum(int typeList);

    @Query("select count(id) from Releases where results is null and typeList =?1 and secondTransferType =?2 ")
    int findCountNumAndType(int typeList,int secondTransferType);
}
