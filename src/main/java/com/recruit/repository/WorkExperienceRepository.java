package com.recruit.repository;

import com.recruit.base.BaseRepository;
import com.recruit.entity.WorkExperience;

import java.util.List;

public interface WorkExperienceRepository extends BaseRepository<WorkExperience,Long> {

        List<WorkExperience> findByJobInformationId(Long jobInformationId);
}
