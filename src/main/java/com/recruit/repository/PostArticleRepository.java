package com.recruit.repository;

import com.recruit.base.BaseRepository;
import com.recruit.entity.PostArticle;
import org.springframework.data.jpa.repository.Query;

public interface PostArticleRepository extends BaseRepository<PostArticle,Long> {

    @Query("select count(id) from PostArticle where results is null")
    int findCountNum();
}
