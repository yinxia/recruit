package com.recruit.repository;

import com.recruit.base.BaseRepository;
import com.recruit.entity.Display;

public interface DisplayRepository extends BaseRepository<Display,Long> {

}
