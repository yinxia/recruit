package com.recruit.services;


import com.recruit.base.BaseServices;
import com.recruit.entity.Members;
import com.recruit.entity.Order;
import com.recruit.entity.User;
import com.recruit.pager.Condition;
import com.recruit.pager.Pager;
import com.recruit.pager.RestrictionType;
import com.recruit.repository.OrderRepository;
import com.recruit.utils.BeanTransform;
import com.recruit.utils.ObjectTools;
import com.recruit.utils.UserContext;
import com.recruit.vo.OrderVo;
import lombok.Data;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Transactional
@Log4j
public class OrderService extends BaseServices<Order,Long> {

    protected OrderRepository repository;

    public OrderService(OrderRepository repository) {
        super(repository);
        this.repository=repository;
    }

    @Autowired
    private UserService userService;

    @Autowired
    private MembersService membersService;

    
    public Order saveOrUpdate(Order order, Members members){

        Map<String, Object> getUserInfo = UserContext.getUser();
        Calendar cd = Calendar.getInstance();
        cd.setTime(new Date());
        order.setStartTime(new Date());
        if(null != getUserInfo){
            order.setUserId(ObjectTools.toLong(getUserInfo.get("id")));
        }
        if(members.getMembersType().equals("1")){
            cd.add(Calendar.DATE, 30);
            order.setEndTime(cd.getTime());
            order.setDays(30);
        }else {
            cd.add(Calendar.YEAR, 1);
            order.setEndTime(cd.getTime());
            order.setDays(365);
        }
        order.setPayOrNot("2");
        this.saveOrUpdate(order);

        User user = userService.findOne(ObjectTools.toLong(getUserInfo.get("id")));
        if(null != user){
            user.setUserType("1");
            userService.update(user);
        }
        return order;
    }

    public Order membersAuthorization(Order order,String phone){

        Calendar cd = Calendar.getInstance();
        User user = userService.findByphone(phone);
        if(null != user){
            Order order1 = this.findByUserId(user.getId());
            if(null != order1){
                if(!ObjectTools.isNull(order1.getStartTime()) && !ObjectTools.isNull(order1.getEndTime())){
                    cd.setTime(order1.getEndTime());
                    cd.add(Calendar.DATE,order.getDays());
                    order1.setEndTime(cd.getTime());
                    order1.setDays(order1.getDays() + order.getDays());
                }
                this.update(order1);

            }else {

                Order o = new Order();
                o.setStartTime(new Date());
                cd.setTime(new Date());
                cd.add(Calendar.DATE,order.getDays());
                o.setEndTime(cd.getTime());
                o.setDays(order.getDays());
                o.setUserId(user.getId());
                o.setMembersId(order.getMembersId());
                o.setPayOrNot("1");
                this.save(o);

                user.setUserType("1");
                userService.update(user);
            }
        }
        return order;
    }


    public Order findByUserId(Long userId){
        return repository.findByUserId(userId);
    }


    public String findSql(OrderVo vo, Date startTime,Date endTime){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd-HH:mm:ss");

        String sql = "SELECT re1.*,re2.vx_number AS vx_number1,re2.phone,re3.money2,re3.members_type \n" +
                "FROM `re_order` re1 LEFT JOIN `re_user` re2 ON re1.user_id = re2.id\n" +
                "LEFT JOIN `members` re3 ON re1.`members_id` = re3.`id` where 1=1 ";


        if(!ObjectTools.isNull(startTime)) {
            sql += " and re1.create_date >=  '" + sdf.format(startTime) + "'  ";
        }

        if(!ObjectTools.isNull(endTime)) {
            sql += " and re1.create_date <=  '" + sdf.format(endTime) + "'  ";
        }

        if(!ObjectTools.isNull(vo.getUserPhone())) {
            sql += " and re2.phone = " + vo.getUserPhone() + "  ";
        }

        if(!ObjectTools.isNull(vo.getUserVxNumber())) {
            sql += " and re2.vx_number like '%" + vo.getUserVxNumber() + "%'  ";
        }

        if(!ObjectTools.isNull(vo.getPayOrNot())) {
            sql += " and re1.pay_or_not = " + vo.getPayOrNot() + " ";
        }

        return sql;
    }

    public Pager<OrderVo> pagerList(Pager<Order> pager, OrderVo vo, Date startTime,Date endTime){

        String sql = findSql(vo,startTime,endTime);

        StringBuffer hqlBuffer = new StringBuffer(sql);
        StringBuffer countHql = new StringBuffer(sql);

        this.appendHsql(pager, hqlBuffer, countHql, true);
        this.findPagerBySql(pager, hqlBuffer.toString()+ " order by re1.create_date desc ",
                "select count(1) from (" + hqlBuffer.toString() + ") b " ,false);


        Pager<OrderVo> pagerVo = pager.copy();
        List<OrderVo> voList = new Vector<>();
        List<Order> list = pager.getContent();
        list.stream().forEach(k ->{
            OrderVo vo1 = findOrderVo(k);
            voList.add(vo1);
        });
        pagerVo.setContent(voList);

        return pagerVo;
    }


    public OrderVo findOrderVo(Order order){

        OrderVo vo = BeanTransform.copyProperties(order,OrderVo.class);
        vo.setId(order.getId() + "");
        vo.setCreateDate(order.getCreateDate());

        User user = userService.findOne(order.getUserId());
        if(null != user){
            vo.setUserPhone(user.getPhone());
            vo.setUserVxNumber(user.getVxNumber());
        }

        Members members = membersService.findOne(order.getMembersId());
        if(null != members){
            vo.setMembersName(members.getMembersType().equals("1") ? "月" : "年");
            vo.setMoney2(members.getMoney2());
        }
        return vo;
    }


    public String sumMoney(OrderVo vo, Date startTime,Date endTime){

        String sql = "SELECT SUM(b.money2) AS money FROM ( ";

        sql  += findSql(vo,startTime,endTime) + ") b ";

        List<Map<String, Object>> maps = super.findListForSqlList(sql);
        String money = null;
        for(Map<String,Object> map : maps){
            money = map.get("money") + "";
        }
        money = money.equals("null") ? "0.0" : money;

        return money;
    }



}
