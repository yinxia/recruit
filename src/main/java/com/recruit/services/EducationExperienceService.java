package com.recruit.services;

import com.recruit.base.BaseServices;
import com.recruit.entity.EducationExperience;
import com.recruit.repository.EducationExperienceRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class EducationExperienceService extends BaseServices<EducationExperience,Long> {

    protected EducationExperienceRepository repository;

    public EducationExperienceService(EducationExperienceRepository repository) {
        super(repository);
        this.repository=repository;
    }


    public List<EducationExperience> findByJobInformationId(Long jobInformationId){
        return repository.findByJobInformationId(jobInformationId);
    }

}
