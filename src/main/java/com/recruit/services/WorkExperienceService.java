package com.recruit.services;

import com.recruit.base.BaseServices;
import com.recruit.entity.WorkExperience;
import com.recruit.repository.WorkExperienceRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class WorkExperienceService  extends BaseServices<WorkExperience,Long> {

    protected WorkExperienceRepository repository;

    public WorkExperienceService(WorkExperienceRepository repository) {
        super(repository);
        this.repository=repository;
    }

    public List<WorkExperience> findByJobInformationId(Long jobInformationId){
        return repository.findByJobInformationId(jobInformationId);
    }
}
