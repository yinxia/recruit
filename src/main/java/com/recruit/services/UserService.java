package com.recruit.services;

import com.recruit.base.BaseServices;
import com.recruit.entity.Members;
import com.recruit.entity.Order;
import com.recruit.entity.User;
import com.recruit.pager.Condition;
import com.recruit.pager.Pager;
import com.recruit.pager.RestrictionType;
import com.recruit.repository.UserRepository;
import com.recruit.utils.BeanTransform;
import com.recruit.utils.MD5;
import com.recruit.utils.ObjectTools;
import com.recruit.vo.UserOrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;

/**
 * @ClassName: StoreUserService
 * @Author: pengyongliang
 * @Date: 19-6-27 下午2:50
 * @Desription:
 **/
@Service
@Transactional
public class UserService extends BaseServices<User,Long>{

    protected UserRepository repository;

    @Autowired
    private OrderService orderService;

    @Autowired
    private MembersService membersService;

    public UserService(UserRepository repository) {
        super(repository);
        this.repository=repository;
    }


    public User findByphone(String phone){
        return repository.findByPhone(phone);
    }


    public User register(User user) {

        user.setUName(user.getPhone());
        //String code = RandomUtils.verifyUserName(2, 8);
        user.setPassword(MD5.md5(user.getPassword()));
        user.setPhone(user.getPhone());
        user.setUpdateDate(new Date());
        user.setCreateDate(new Date());
        repository.save(user);

        return user;
    }



    public int updateLoginTime(Date time, String phone) {
        return repository.updateLoginTime(time, phone);
    }


    public UserOrderVo getUserOrderVo(User user){

        UserOrderVo userOrderVo = BeanTransform.copyProperties(user,UserOrderVo.class);
        userOrderVo.setId(user.getId());
        userOrderVo.setUName(user.getUName());
        Order order = orderService.findByUserId(user.getId());
        if(null != order){
            userOrderVo.setDays(order.getDays());
            userOrderVo.setStartTime(order.getStartTime());
            userOrderVo.setEndTime(order.getEndTime());
            userOrderVo.setMembersId(order.getMembersId() == null ? 0L : order.getMembersId());
            userOrderVo.setUndoTime(order.getUndoTime());
            Members members = membersService.findOne(order.getMembersId());
            if(null != members){
                if(members.getMembersType().equals("1")){
                    userOrderVo.setMembersName("月VIP会员");
                }else {
                    userOrderVo.setMembersName("年VIP会员");
                }
            }
        }
        return userOrderVo;
    }


    public Pager<UserOrderVo> getPagerAllList(Pager<User> pager, UserOrderVo userOrderVo){

        if(!ObjectTools.isNull(userOrderVo.getPhone())){
            pager.addCondition(new Condition("phone", userOrderVo.getPhone(), RestrictionType.EQ));
        }

        if(!ObjectTools.isNull(userOrderVo.getVxNumber())){
            pager.addCondition(new Condition("vxNumber", userOrderVo.getVxNumber(), RestrictionType.EQ));
        }

        if(!ObjectTools.isNull(userOrderVo.getUName())){
            pager.addCondition(new Condition("uName", userOrderVo.getUName(), RestrictionType.LIKE));
        }

        pager.addOrder(new Pager.Order("createDate", Pager.OrderType.DESC));

        this.findPager(pager);
        Pager<UserOrderVo> pagerVo = pager.copy();
        for (User user : pager.getContent()) {
            UserOrderVo vo = getUserOrderVo(user);
            pagerVo.getContent().add(vo);
        }

        return pagerVo;
    }

}
