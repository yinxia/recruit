package com.recruit.services;

import com.recruit.base.BaseServices;
import com.recruit.entity.*;
import com.recruit.pager.Pager;
import com.recruit.repository.FactoryRecruitmentRepository;
import com.recruit.utils.BeanTransform;
import com.recruit.utils.ObjectTools;
import com.recruit.utils.UploadUtils;
import com.recruit.utils.UserContext;
import com.recruit.vo.FactoryRecruitmentVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class FactoryRecruitmentService extends BaseServices<FactoryRecruitment,Long> {

    protected FactoryRecruitmentRepository repository;

    public FactoryRecruitmentService(FactoryRecruitmentRepository repository) {
        super(repository);
        this.repository=repository;
    }

    @Autowired
    private FactoryRecruitmentTypeService factoryRecruitmentTypeService;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private InfoUploadFileService fileService;

    @Autowired
    private UserService userService;


    public FactoryRecruitment saveOrUpdate(String json,FactoryRecruitment fr){


        JsonParser jsonParser = JsonParserFactory.getJsonParser();
        Map<String, Object> map = jsonParser.parseMap(json);
        Set<String> uploadPhotoSet = new HashSet<>();

        FactoryRecruitment factoryRecruitment = BeanTransform.map2Entity(map,FactoryRecruitment.class);
        FactoryRecruitment factoryRecruitment1 = null;

        Map<String, Object> getUserInfo = UserContext.getUser();
        if(null != getUserInfo){
            factoryRecruitment.setUserId(ObjectTools.toLong(getUserInfo.get("id")));
        }

        if(map.get("id") != null && ObjectTools.toLong(map.get("id")) != 0){
            factoryRecruitment1 = this.findOne(factoryRecruitment.getId());
            uploadPhotoSet = new HashSet<>(StringUtils.isEmpty(factoryRecruitment1.getUploadPhoto()) ? new HashSet<>() : Arrays.asList(factoryRecruitment1.getUploadPhoto().split(",")));
        }else{

            this.saveOrUpdate(factoryRecruitment);
        }

        //企业场景
        if(fr.getUploadPhotos() != null && fr.getUploadPhotos().size() != 0){
            List<InfoUploadFile> list = editInfo(uploadPhotoSet, fr.getUploadPhotos(),factoryRecruitment,map.get("title")+"");
            factoryRecruitment.setUploadPhotos(list);
            factoryRecruitment.setUploadPhoto(StringUtils.join(list.stream().map(InfoUploadFile::getId).collect(Collectors.toList()), ","));
        }else{
            factoryRecruitment.setUploadPhotos(new ArrayList<>());
            factoryRecruitment.setUploadPhoto("");
        }

        this.saveOrUpdate(factoryRecruitment);


        Set<Long> longs = new HashSet<>();
        if(!ObjectTools.isNull(map.get("typeLists"))) {
            List<Map<String, Object>> columns = (List<Map<String, Object>>) map.get("typeLists");

            columns.stream().forEach(v ->{
                FactoryRecruitmentType factoryRecruitmentType = BeanTransform.map2Entity(v,FactoryRecruitmentType.class);
                factoryRecruitmentType.setFactoryRecruitmentId(factoryRecruitment.getId());
                factoryRecruitmentTypeService.saveOrUpdate(factoryRecruitmentType);
                longs.add(factoryRecruitmentType.getId());
            });

            //不存在的id就删除
            List<FactoryRecruitmentType> factoryRecruitmentTypes = factoryRecruitmentTypeService.findByFactoryRecruitmentId(ObjectTools.toLong(map.get("id")));
            factoryRecruitmentTypes.stream().forEach(k ->{
                if(!longs.contains(k.getId())){
                    factoryRecruitmentTypeService.delete(k.getId());
                }
            });
        }

        //删除被删除的文件
        if(uploadPhotoSet.size() != 0){
            deleteFile(uploadPhotoSet);
        }

        return factoryRecruitment;
    }


    public void deleteFile( Set<String> set ){
        Long [] ids = new Long[set.size()];
        for(int i = 0 ; i < set.size() ; i++){
            ids[i] = Long.parseLong(set.toArray()[i]+"");
        }
        fileService.deletes(ids);
    }

    public  List<InfoUploadFile> editInfo (Set<String> set, List<InfoUploadFile> vo,FactoryRecruitment factoryRecruitment,String title){
        List<InfoUploadFile> fileList = new ArrayList<>();
        for(int i = 0 ; i < vo.size() ; i++){
            InfoUploadFile restInfoUploadFileVo = vo.get(i);
            if(restInfoUploadFileVo.getId() != null && restInfoUploadFileVo.getId() != 0){
                set.remove(restInfoUploadFileVo.getId()+"");
                InfoUploadFile file = fileService.findOne(restInfoUploadFileVo.getId());
                if(file != null ){
                    //file.setVal(restInfoUploadFileVo.getVal());
                    restInfoUploadFileVo = fileService.saveOrUpdate(file);
                }
            }else{
                MultipartFile file = restInfoUploadFileVo.getFile();
                try {
                    restInfoUploadFileVo = fileService.upload2(UploadUtils.convertFile(file),restInfoUploadFileVo.getVal(),"发布/"+title,factoryRecruitment.getId());
                }catch (Exception e) {
                    System.out.println(e);
                    throw new RuntimeException("上传文件失败！");
                }
            }
            restInfoUploadFileVo.setFile(null);
            fileList.add(restInfoUploadFileVo);
        }
        //redisService.del("dirsTree");
        return fileList;
    }



    public Pager<FactoryRecruitment> getPagerAllList(Pager<FactoryRecruitment> pager, FactoryRecruitmentVo vo){


        String sql = " SELECT a.* FROM (SELECT \n" +
                " f1.*,\n" +
                " f3.type type1,\n" +
                " f3.`city`,f3.`area`,f3.`province`\n" +
                "FROM\n" +
                "  `re_factory_recruitment` f1 \n" +
                //"  LEFT JOIN `re_factory_recruitment_type` f2 \n" +
                //"    ON f1.id = f2.factoryrecruitment_id \n" +
                "  LEFT JOIN `re_authentication` f3 \n" +
                "    ON f3.`user_id` = f1.user_id\n" +
                "    WHERE 1=1\n";

        if(StringUtils.isNotBlank(vo.getAuthenticationType())){
            sql += " AND f3.`type` = '" + vo.getAuthenticationType() + "'";
        }

        if(StringUtils.isNotBlank(vo.getDistance())){
            sql += " AND f1.distance = '" + vo.getDistance() + "'  ";
        }

        if(StringUtils.isNotBlank(vo.getResults())){
            sql += " AND f1.results = '" + vo.getResults() + "'  ";
        }

        sql += " ) a ";
        /*int a = 0;
        if(null != types && types.length > 0){
            for(String type : types){
                if(a == 0) {
                    sql += " where a.name = '" + type + "'";
                    a++;
                    continue;
                }
                if(a > 0) {
                    sql += " or a.name = '" + type + "'";
                }
            }
        }*/

        StringBuffer hqlBuffer = new StringBuffer(sql);
        StringBuffer countHql = new StringBuffer(sql);

        this.appendHsql(pager, hqlBuffer, countHql, true);
        this.findPagerBySql(pager, hqlBuffer.toString()+ " order by a.create_date desc ",
                "select count(1) from (" + hqlBuffer.toString() + ") a " ,false);


        //Pager<FactoryRecruitmentVo> pagerVo = pager.copy();
        /*List<Map<String, Object>> maps = super.findListForSqlList(hqlBuffer.toString());
        List<FactoryRecruitmentVo> vos = new ArrayList<>();

        maps.stream().forEach(k ->{
            FactoryRecruitmentVo factoryRecruitmentVo = BeanTransform.map2Entity(k,FactoryRecruitmentVo.class);
            vos.add(factoryRecruitmentVo);
        });*/
        //pagerVo.setContent(vos);

        return pager;
    }


    public Pager<FactoryRecruitmentVo> getPagerAllList2(Pager<FactoryRecruitment> pager, FactoryRecruitmentVo vo,String [] types){

        getPagerAllList(pager,vo);
        Pager<FactoryRecruitmentVo> voPager = pager.copy();
        List<FactoryRecruitmentVo> frVo = new Vector<>();
        List<FactoryRecruitment> factoryRecruitments = pager.getContent();
        factoryRecruitments.stream().forEach(k ->{
            FactoryRecruitmentVo factoryRecruitmentVo =  BeanTransform.copyProperties(k,FactoryRecruitmentVo.class);
            factoryRecruitmentVo.setId(k.getId()+"");
            factoryRecruitmentVo.setCreateDate(k.getCreateDate());
            User user = userService.findOne(k.getCreateBy());
            if(null != user){
                factoryRecruitmentVo.setUserPhone(user.getPhone());
                factoryRecruitmentVo.setUserVxNumber(user.getVxNumber());
            }
            if(StringUtils.isNotBlank(k.getStartSalary()) && StringUtils.isNotBlank(k.getEndSalary())){
                factoryRecruitmentVo.setSalary(k.getStartSalary() + "-" + k.getEndSalary());
            }else {
                factoryRecruitmentVo.setSalary("面议");
            }
            Authentication authentication = authenticationService.findByUserId(k.getCreateBy());
            if(null != authentication){
                factoryRecruitmentVo.setCity(authentication.getCity());
                factoryRecruitmentVo.setArea(authentication.getArea());
                factoryRecruitmentVo.setProvince(authentication.getProvince());
                factoryRecruitmentVo.setAuthenticationType(authentication.getType());
                List<FactoryRecruitmentType> types1 = factoryRecruitmentTypeService.findByFactoryRecruitmentId(k.getId());
                List<FactoryRecruitmentType> types2 = new ArrayList<>();
                if(null != types){
                    if(null != types1) {
                       if(containArray(types1,types)){
                           factoryRecruitmentVo.setVoList(types1);
                           frVo.add(factoryRecruitmentVo);
                       }
                    }
                }else if(null != types1){
                    types1.stream().forEach(v ->{
                        types2.add(v);
                    });
                    factoryRecruitmentVo.setVoList(types2);
                    frVo.add(factoryRecruitmentVo);
                }
            }
        });
        voPager.setContent(frVo);
        voPager.setTotalElements(frVo.size());
        return voPager;
    }




    public Boolean containArray(List<FactoryRecruitmentType> typeList,String [] types) {

        for(FactoryRecruitmentType factoryRecruitmentType : typeList){
            if(Arrays.asList(types).contains(factoryRecruitmentType.getName())){
                return true;
            }
        }
        return false;
    }

    public FactoryRecruitment details(Long id){

        FactoryRecruitment factoryRecruitment = this.findOne(id);
        if(null != factoryRecruitment){
            List<FactoryRecruitmentType> typeList = factoryRecruitmentTypeService.findByFactoryRecruitmentId(factoryRecruitment.getId());
            factoryRecruitment.setFactoryRecruitmentTypes(typeList == null ? new ArrayList<>() : typeList);
            Authentication authentication = authenticationService.findByUserId(factoryRecruitment.getCreateBy());
            if(null != authentication){
                factoryRecruitment.setAuthentication(authentication);
            }
        }
        return factoryRecruitment;
    }

    public FactoryRecruitment updateResults(FactoryRecruitment factoryRecruitment){

        FactoryRecruitment factoryRecruitment1 = this.findOne(factoryRecruitment.getId());

        if(null != factoryRecruitment1 && factoryRecruitment.getResults().equals("通过")){
            factoryRecruitment1.setResults("通过");
            factoryRecruitment1.setCause("");
            factoryRecruitment1.setOtherContent("");
        }

        if(null != factoryRecruitment1 && factoryRecruitment.getResults().equals("不通过")){
            factoryRecruitment1.setResults("不通过");
            factoryRecruitment1.setCause(factoryRecruitment.getCause());
            factoryRecruitment1.setOtherContent(factoryRecruitment.getOtherContent());
        }

        if(!factoryRecruitment1.getCause().equals("其他") && !factoryRecruitment1.getOtherContent().equals("") && null != factoryRecruitment1.getOtherContent()){
            factoryRecruitment1.setOtherContent("");
        }

        this.update(factoryRecruitment1);

        return factoryRecruitment1;
    }


    public Pager<FactoryRecruitmentVo> pagerList(Pager<FactoryRecruitment> pager,FactoryRecruitmentVo vo,Date startTime,Date endTime){


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd-HH:mm:ss");

        String sql = "SELECT re1.*,re2.vx_number as vx_number1,re2.phone as phone1 FROM `re_factory_recruitment`  re1  LEFT JOIN `re_user` as re2  \n" +
                     "ON re1.`create_by` = re2.id where 1=1 ";

        if(!ObjectTools.isNull(startTime)) {
            sql += " and re1.create_date >=  '" + sdf.format(startTime) + "'  ";
        }

        if(!ObjectTools.isNull(endTime)) {
            sql += " and re1.create_date <=  '" + sdf.format(endTime) + "'  ";
        }

        if(!ObjectTools.isNull(vo.getUserPhone())) {
            sql += " and re2.phone = " + vo.getUserPhone() + "  ";
        }

        if(!ObjectTools.isNull(vo.getUserVxNumber())) {
            sql += " and re2.vx_number like '%" + vo.getUserVxNumber() + "%'  ";
        }

        if(!ObjectTools.isNull(vo.getResults())) {
            sql += " and re1.results = '" + vo.getResults() + "'  ";
        }

        if(!ObjectTools.isNull(vo.getRecruitType())) {
            sql += " and re1.recruit_type = " + vo.getRecruitType() + "  ";
        }

        StringBuffer hqlBuffer = new StringBuffer(sql);
        StringBuffer countHql = new StringBuffer(sql);

        this.appendHsql(pager, hqlBuffer, countHql, true);
        this.findPagerBySql(pager, hqlBuffer.toString()+ " order by re1.create_date desc ",
                "select count(1) from (" + hqlBuffer.toString() + ") b " ,false);

        Pager<FactoryRecruitmentVo> pagerVo = pager.copy();
        List<FactoryRecruitmentVo> voList = new Vector<>();
        List<FactoryRecruitment> list = pager.getContent();

        list.stream().forEach(k ->{
            FactoryRecruitmentVo vo1 = findRecruitmentVo(k);
            voList.add(vo1);
        });
        pagerVo.setContent(voList);

        return pagerVo;
    }



    public FactoryRecruitmentVo findRecruitmentVo(FactoryRecruitment factoryRecruitment){

        FactoryRecruitmentVo factoryRecruitmentVo = BeanTransform.copyProperties(factoryRecruitment,FactoryRecruitmentVo.class);
        factoryRecruitmentVo.setId(factoryRecruitment.getId()+"");
        factoryRecruitmentVo.setCreateDate(factoryRecruitment.getCreateDate());
        User user = userService.findOne(factoryRecruitment.getUserId());

        if(null != user){
            factoryRecruitmentVo.setUserPhone(user.getPhone());
            factoryRecruitmentVo.setUserVxNumber(user.getVxNumber());
        }

        if(StringUtils.isNotBlank(factoryRecruitment.getStartSalary()) && StringUtils.isNotBlank(factoryRecruitment.getEndSalary())){
            factoryRecruitmentVo.setSalary(factoryRecruitment.getStartSalary() + "-" + factoryRecruitment.getEndSalary());
        }else {
            factoryRecruitmentVo.setSalary("面议");
        }

        Authentication authentication = authenticationService.findByUserId(factoryRecruitment.getCreateBy());
        if(null != authentication){
            factoryRecruitmentVo.setAuthenticationType(authentication.getType());
        }
        List<FactoryRecruitmentType> types1 = factoryRecruitmentTypeService.findByFactoryRecruitmentId(factoryRecruitment.getId());
        StringBuffer bf = new StringBuffer();
        if(factoryRecruitment.getRecruitType().equals("4")){
            if(null != types1 && types1.size() > 0){
                types1.stream().forEach(k ->{
                    if(null != k.getContent() && !k.getContent().equals("")){
                        bf.append( k.getFunctions()+" : "+k.getContent() +k.getNumber() + "人" );
                        bf.append("\r\n");
                    }else {
                        bf.append( k.getFunctions()+" : "+k.getJob() +k.getNumber() + "人" );
                        bf.append("\r\n");
                    }
                });
                factoryRecruitmentVo.setJobs(bf+"");
            }
        }else {
            if(null != types1 && types1.size() > 0){
                types1.stream().forEach(k ->{
                    bf.append(k.getType()+" : "+k.getName() +k.getNumber() + "人" );
                    bf.append("\r\n");
                });
                factoryRecruitmentVo.setJobs(bf+"");
            }
        }

        return factoryRecruitmentVo;
    }

    public Boolean batchResults(Long ids[]){

        Arrays.stream(ids).forEach(k ->{
            FactoryRecruitment factoryRecruitment = this.findOne(k);
            if(null != factoryRecruitment){
                factoryRecruitment.setResults("通过");
                factoryRecruitment.setCause("");
                factoryRecruitment.setOtherContent("");
                this.update(factoryRecruitment);
            }
        });
        return true;
    }

    public int findCountResults(){
        return repository.findCountResults();
    }

    public int findCountRecruitType(String type){
        return repository.findCountRecruitType(type);
    }

}
