package com.recruit.services;

import com.recruit.base.BaseServices;
import com.recruit.entity.Authentication;
import com.recruit.entity.InfoUploadFile;
import com.recruit.entity.User;
import com.recruit.pager.Pager;
import com.recruit.repository.AuthenticationRepository;
import com.recruit.utils.BeanTransform;
import com.recruit.utils.ObjectTools;
import com.recruit.utils.UploadUtils;
import com.recruit.utils.UserContext;
import com.recruit.vo.AuthenticationVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class AuthenticationService extends BaseServices<Authentication,Long> {

    protected AuthenticationRepository repository;

    public AuthenticationService(AuthenticationRepository repository) {
        super(repository);
        this.repository=repository;
    }


    @Autowired
    private InfoUploadFileService fileService;

    @Autowired
    private UserService userService;


    public Authentication saveOrUpdate(Authentication authentication) {

        Authentication authentication1 = null;

        Map<String, Object> getUserInfo = UserContext.getUser();
        if(null != getUserInfo){
            authentication.setUserId(ObjectTools.toLong(getUserInfo.get("id")));
        }

        Set<String> businessLicenseIds = new HashSet<>();
        Set<String> idPhotoIds = new HashSet<>();

        if(authentication.getId() != null && authentication.getId() != 0){
            authentication1 = this.findOne(authentication.getId());
            if(authentication1.getType().equals("1")) {
                businessLicenseIds = new HashSet<>(StringUtils.isEmpty(authentication1.getBusinessScenarios()) ? new HashSet<>() : Arrays.asList(authentication1.getBusinessScenarios().split(",")));
            }
            if(authentication1.getType().equals("2")) {
                idPhotoIds = new HashSet<>(StringUtils.isEmpty(authentication1.getIdPhoto()) ? new HashSet<>() : Arrays.asList(authentication1.getIdPhoto().split(",")));
            }
        }else{
            super.saveOrUpdate(authentication);
        }

        //企业场景
        if(authentication.getBusinessScenarios1() != null && authentication.getBusinessScenarios1().size() != 0){
            List<InfoUploadFile> list = editInfo(businessLicenseIds, authentication.getBusinessScenarios1(),authentication,authentication.getEnterpriseName());
            authentication.setBusinessScenarios1(list);
            authentication.setBusinessScenarios(StringUtils.join(list.stream().map(InfoUploadFile::getId).collect(Collectors.toList()), ","));
        }else{
            authentication.setBusinessScenarios1(new ArrayList<>());
            authentication.setBusinessScenarios("");
        }

        //身份照片
        if(authentication.getIdPhotos() != null && authentication.getIdPhotos().size() != 0){
            List<InfoUploadFile> list = editInfo(idPhotoIds, authentication.getIdPhotos(),authentication,authentication.getEnterpriseName());
            authentication.setBusinessScenarios1(list);
            authentication.setBusinessScenarios(StringUtils.join(list.stream().map(InfoUploadFile::getId).collect(Collectors.toList()), ","));
        }else{
            authentication.setBusinessScenarios1(new ArrayList<>());
            authentication.setBusinessScenarios("");
        }
        super.saveOrUpdate(authentication);

        //删除被删除的文件
        if(businessLicenseIds.size() != 0){
            deleteFile(businessLicenseIds);
        }

        if(idPhotoIds.size() != 0){
            deleteFile(idPhotoIds);
        }

        return authentication;
    }


    public  List<InfoUploadFile> editInfo (Set<String> set, List<InfoUploadFile> vo,Authentication authentication,String title){
        List<InfoUploadFile> fileList = new ArrayList<>();
        for(int i = 0 ; i < vo.size() ; i++){
            InfoUploadFile restInfoUploadFileVo = vo.get(i);
            if(restInfoUploadFileVo.getId() != null && restInfoUploadFileVo.getId() != 0){
                set.remove(restInfoUploadFileVo.getId()+"");
                InfoUploadFile file = fileService.findOne(restInfoUploadFileVo.getId());
                if(file != null ){
                    //file.setVal(restInfoUploadFileVo.getVal());
                    restInfoUploadFileVo = fileService.saveOrUpdate(file);
                }
            }else{
                MultipartFile file = restInfoUploadFileVo.getFile();
                try {
                    restInfoUploadFileVo = fileService.upload2(UploadUtils.convertFile(file),restInfoUploadFileVo.getVal(),"认证/"+title,authentication.getId());
                }catch (Exception e) {
                    System.out.println(e);
                    throw new RuntimeException("上传文件失败！");
                }
            }
            restInfoUploadFileVo.setFile(null);
            fileList.add(restInfoUploadFileVo);
        }
        //redisService.del("dirsTree");
        return fileList;
    }


    public void deleteFile( Set<String> set ){
        Long [] ids = new Long[set.size()];
        for(int i = 0 ; i < set.size() ; i++){
            ids[i] = Long.parseLong(set.toArray()[i]+"");
        }
        fileService.deletes(ids);
    }


    public Authentication details(Long id){

        Authentication authentication = this.findOne(id);
        List<InfoUploadFile>  vo1 = new ArrayList<>();
        if(null != authentication){

            if(authentication.getType().equals("1")) {
                Set<String> list1 = new HashSet<>(StringUtils.isEmpty(authentication.getBusinessScenarios()) ? new HashSet<>() : Arrays.asList(authentication.getBusinessScenarios().split(",")));
                if (list1.size() > 0) {
                    list1.stream().forEach(k -> {
                        InfoUploadFile restInfoUploadFileVo = fileService.findOne(ObjectTools.toLong(k));
                        if (restInfoUploadFileVo != null && restInfoUploadFileVo.getId() != null)
                            vo1.add(restInfoUploadFileVo);
                    });
                }
                authentication.setBusinessScenarios1(vo1);
            }

            if(authentication.getType().equals("2")) {
                Set<String> list1 = new HashSet<>(StringUtils.isEmpty(authentication.getIdPhoto()) ? new HashSet<>() : Arrays.asList(authentication.getIdPhoto().split(",")));
                if (list1.size() > 0) {
                    list1.stream().forEach(k -> {
                        InfoUploadFile restInfoUploadFileVo = fileService.findOne(ObjectTools.toLong(k));
                        if (restInfoUploadFileVo != null && restInfoUploadFileVo.getId() != null)
                            vo1.add(restInfoUploadFileVo);
                    });
                }
                authentication.setIdPhotos(vo1);
            }
        }

        if(authentication.getType().equals("1")) {
            InfoUploadFile restInfoUploadFileVo = fileService.findOne(ObjectTools.toLong(authentication.getBusinessLicense()));
            if (restInfoUploadFileVo != null && restInfoUploadFileVo.getId() != null) {
                authentication.setBusinessLicenses(restInfoUploadFileVo);
            }
        }

        return authentication;
    }


    public Pager<AuthenticationVo> pagerList(Pager<Authentication> pager, AuthenticationVo vo, Date startTime, Date endTime){


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd-HH:mm:ss");

        String sql = "SELECT re1.*,re2.vx_number AS vx_number1,re2.phone AS phone1 FROM `re_authentication` re1\n" +
                "LEFT JOIN `re_user` AS re2 ON re1.user_id = re2.id WHERE 1=1 ";

        if(!ObjectTools.isNull(startTime)) {
            sql += " and re1.create_date >=  '" + sdf.format(startTime) + "'  ";
        }

        if(!ObjectTools.isNull(endTime)) {
            sql += " and re1.create_date <=  '" + sdf.format(endTime) + "'  ";
        }

        if(!ObjectTools.isNull(vo.getUserPhone())) {
            sql += " and re2.phone = " + vo.getUserPhone() + "  ";
        }

        if(!ObjectTools.isNull(vo.getUserVxNumber())) {
            sql += " and re2.vx_number like '%" + vo.getUserVxNumber() + "%'  ";
        }

        if(!ObjectTools.isNull(vo.getResults())) {
            sql += " and re1.results = '" + vo.getResults() + "'  ";
        }

        if(!ObjectTools.isNull(vo.getType())) {
            sql += " and re1.type = " + vo.getType() + "  ";
        }

        StringBuffer hqlBuffer = new StringBuffer(sql);
        StringBuffer countHql = new StringBuffer(sql);

        this.appendHsql(pager, hqlBuffer, countHql, true);
        this.findPagerBySql(pager, hqlBuffer.toString()+ " order by re1.create_date desc ",
                "select count(1) from (" + hqlBuffer.toString() + ") b " ,false);

        Pager<AuthenticationVo> pagerVo = pager.copy();
        List<AuthenticationVo> voList = new Vector<>();
        List<Authentication> list = pager.getContent();
        list.stream().forEach(k ->{
            AuthenticationVo vo1 = findAuthenticationVo(k);
            voList.add(vo1);
        });
        pagerVo.setContent(voList);

        return pagerVo;

    }

    public AuthenticationVo findAuthenticationVo(Authentication authentication){

        AuthenticationVo vo = BeanTransform.copyProperties(authentication,AuthenticationVo.class);
        vo.setId(authentication.getId() + "");
        vo.setCreateDate(authentication.getCreateDate());

        User user = userService.findOne(authentication.getUserId());
        if(null != user){
            vo.setUserPhone(user.getPhone());
            vo.setUserVxNumber(user.getVxNumber());
        }
        return vo;
    }






    public Authentication updateResults(Authentication authentication){

        Authentication authentication1 = this.findOne(authentication.getId());

        if(null != authentication1 && authentication.getResults().equals("通过")){
            authentication1.setResults("通过");
            authentication1.setCause("");
            authentication1.setOtherContent("");
        }

        if(null != authentication1 && authentication.getResults().equals("不通过")){
            authentication1.setResults("不通过");
            authentication1.setCause(authentication.getCause());
            authentication1.setOtherContent(authentication.getOtherContent());
        }

        if(!authentication1.getCause().equals("其他") && !authentication1.getOtherContent().equals("") && null != authentication1.getOtherContent()){
            authentication1.setOtherContent("");
        }

        this.update(authentication1);

        return authentication1;
    }


    public Boolean batchResults(Long ids[]){

        Arrays.stream(ids).forEach(k ->{
            Authentication authentication = this.findOne(k);
            if(null != authentication){
                authentication.setResults("通过");
                authentication.setCause("");
                authentication.setOtherContent("");
                this.update(authentication);
            }
        });
        return true;
    }


    public int findCountResults(){
        return repository.findCountResults();
    }

    public int findCountResultsAndType(String type){
        return repository.findCountResultsAndType(type);
    }

    public Authentication findByUserId(Long id){
        return repository.findByUserId(id);
    }

}
