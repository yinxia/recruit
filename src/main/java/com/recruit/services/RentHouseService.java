package com.recruit.services;

import com.recruit.base.BaseServices;
import com.recruit.entity.Authentication;
import com.recruit.entity.InfoUploadFile;
import com.recruit.entity.RentHouse;
import com.recruit.entity.User;
import com.recruit.pager.Pager;
import com.recruit.repository.RentHouseRepository;
import com.recruit.utils.BeanTransform;
import com.recruit.utils.ObjectTools;
import com.recruit.utils.UploadUtils;
import com.recruit.utils.UserContext;
import com.recruit.vo.RentHouseVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class RentHouseService extends BaseServices<RentHouse,Long> {

    protected RentHouseRepository repository;

    public RentHouseService(RentHouseRepository repository) {
        super(repository);
        this.repository=repository;
    }

    @Autowired
    private InfoUploadFileService fileService;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationService authenticationService;


    public RentHouse saveOrUpdate(RentHouse rentHouse){

        RentHouse rentHouse1 = null;

        Set<String> photoId = new HashSet<>();

        Map<String, Object> getUserInfo = UserContext.getUser();
        if(null != getUserInfo){
            rentHouse.setUserId(ObjectTools.toLong(getUserInfo.get("id")));
        }

        if(rentHouse.getId() != null && rentHouse.getId() != 0){
            rentHouse1 = this.findOne(rentHouse.getId());
            photoId = new HashSet<>(StringUtils.isEmpty(rentHouse1.getPhotoId()) ? new HashSet<>() : Arrays.asList(rentHouse1.getPhotoId().split(",")));
        }else{
            super.saveOrUpdate(rentHouse);
        }

        //企业图片
        if(rentHouse.getPhotoIds() != null && rentHouse.getPhotoIds().size() != 0){
            List<InfoUploadFile> list = editInfo(photoId, rentHouse.getPhotoIds(),rentHouse,rentHouse.getTitle());
            rentHouse.setPhotoIds(list);
            rentHouse.setPhotoId(StringUtils.join(list.stream().map(InfoUploadFile::getId).collect(Collectors.toList()), ","));
        }else{
            rentHouse.setPhotoIds(new ArrayList<>());
            rentHouse.setPhotoId("");
        }
        super.saveOrUpdate(rentHouse);

        //删除被删除的文件
        if(photoId.size() != 0){
            deleteFile(photoId);
        }

        return rentHouse;
    }


    public  List<InfoUploadFile> editInfo (Set<String> set, List<InfoUploadFile> vo, RentHouse rentHouse, String title){
        List<InfoUploadFile> fileList = new ArrayList<>();
        for(int i = 0 ; i < vo.size() ; i++){
            InfoUploadFile restInfoUploadFileVo = vo.get(i);
            if(restInfoUploadFileVo.getId() != null && restInfoUploadFileVo.getId() != 0){
                set.remove(restInfoUploadFileVo.getId()+"");
                InfoUploadFile file = fileService.findOne(restInfoUploadFileVo.getId());
                if(file != null ){
                    //file.setVal(restInfoUploadFileVo.getVal());
                    restInfoUploadFileVo = fileService.saveOrUpdate(file);
                }
            }else{
                MultipartFile file = restInfoUploadFileVo.getFile();
                try {
                    restInfoUploadFileVo = fileService.upload2(UploadUtils.convertFile(file),restInfoUploadFileVo.getVal(),"租房/"+title,rentHouse.getId());
                }catch (Exception e) {
                    System.out.println(e);
                    throw new RuntimeException("上传文件失败！");
                }
            }
            restInfoUploadFileVo.setFile(null);
            fileList.add(restInfoUploadFileVo);
        }
        //redisService.del("dirsTree");
        return fileList;
    }



    public void deleteFile( Set<String> set ){
        Long [] ids = new Long[set.size()];
        for(int i = 0 ; i < set.size() ; i++){
            ids[i] = Long.parseLong(set.toArray()[i]+"");
        }
        fileService.deletes(ids);
    }


    public Pager<RentHouseVo> pagerList(Pager<RentHouse> pager,RentHouseVo vo,Date startTime,Date endTime){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd-HH:mm:ss");


        String sql = "SELECT re1.*,re2.vx_number AS vx_number1,re2.phone AS phone1 FROM `re_renthouse` re1\n" +
                     "LEFT JOIN `re_user` AS re2 ON re1.user_id = re2.id WHERE 1=1\n ";


        if(!ObjectTools.isNull(startTime)) {
            sql += " and re1.create_date >=  '" + sdf.format(startTime) + "'  ";
        }

        if(!ObjectTools.isNull(endTime)) {
            sql += " and re1.create_date <=  '" + sdf.format(endTime) + "'  ";
        }

        if(!ObjectTools.isNull(vo.getUserPhone())) {
            sql += " and re2.phone = " + vo.getUserPhone() + "  ";
        }

        if(!ObjectTools.isNull(vo.getUserVxNumber())) {
            sql += " and re2.vx_number like '%" + vo.getUserVxNumber() + "%'  ";
        }

        if(!ObjectTools.isNull(vo.getResults())) {
            sql += " and re1.results = '" + vo.getResults() + "'  ";
        }

        StringBuffer hqlBuffer = new StringBuffer(sql);
        StringBuffer countHql = new StringBuffer(sql);

        this.appendHsql(pager, hqlBuffer, countHql, true);
        this.findPagerBySql(pager, hqlBuffer.toString()+ " order by re1.create_date desc ",
                "select count(1) from (" + hqlBuffer.toString() + ") b " ,false);


        Pager<RentHouseVo> pagerVo = pager.copy();
        List<RentHouseVo> voList = new Vector<>();
        List<RentHouse> list = pager.getContent();
        list.stream().forEach(k ->{
            RentHouseVo vo1 = findRentHouseVo(k);
            voList.add(vo1);
        });
        pagerVo.setContent(voList);

        return pagerVo;
    }


    public RentHouseVo findRentHouseVo(RentHouse rentHouse){

        RentHouseVo vo = BeanTransform.copyProperties(rentHouse,RentHouseVo.class);
        vo.setId(rentHouse.getId()+"");
        vo.setCreateDate(rentHouse.getCreateDate());
        if(null == rentHouse.getMoney() && rentHouse.getMoney().equals("")){
            vo.setMoney("面议");
        }

        User user = userService.findOne(vo.getUserId());
        if(null != user){
            vo.setUserPhone(user.getPhone());
            vo.setUserVxNumber(user.getVxNumber());
        }

        Authentication authentication = authenticationService.findByUserId(rentHouse.getUserId());
        if(null != authentication){
            vo.setAuthenticationType(authentication.getType());
        }
        return vo;
    }


    public RentHouse updateResults(RentHouse rentHouse){

        RentHouse rentHouse1 = this.findOne(rentHouse.getId());

        if(null != rentHouse1 && rentHouse.getResults().equals("通过")){
            rentHouse1.setResults("通过");
            rentHouse1.setCause("");
            rentHouse1.setOtherContent("");
        }

        if(null != rentHouse1 && rentHouse.getResults().equals("不通过")){
            rentHouse1.setResults("不通过");
            rentHouse1.setCause(rentHouse.getCause());
            rentHouse1.setOtherContent(rentHouse.getOtherContent());
        }

        if(!rentHouse1.getCause().equals("其他") && !rentHouse1.getOtherContent().equals("") && null != rentHouse1.getOtherContent()){
            rentHouse1.setOtherContent("");
        }

        this.update(rentHouse1);

        return rentHouse1;
    }


    public Boolean batchResults(Long ids[]){

        Arrays.stream(ids).forEach(k ->{
            RentHouse rentHouse = this.findOne(k);
            if(null != rentHouse){
                rentHouse.setResults("通过");
                rentHouse.setCause("");
                rentHouse.setOtherContent("");
                this.update(rentHouse);
            }
        });
        return true;
    }

    public int findCountNum(){
        return repository.findCountNum();
    }

}
