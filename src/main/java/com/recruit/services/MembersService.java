package com.recruit.services;

import com.recruit.base.BaseServices;
import com.recruit.entity.Members;
import com.recruit.entity.User;
import com.recruit.repository.MembersRepository;
import com.recruit.utils.ObjectTools;
import com.recruit.utils.UserContext;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

@Service
@Transactional
@Log4j
public class MembersService extends BaseServices<Members,Long> {

    protected MembersRepository repository;

    public MembersService(MembersRepository repository) {
        super(repository);
        this.repository=repository;
    }



}
