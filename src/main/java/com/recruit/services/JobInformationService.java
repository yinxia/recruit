package com.recruit.services;


import com.recruit.base.BaseServices;
import com.recruit.entity.*;
import com.recruit.pager.Pager;
import com.recruit.repository.JobInformationRepository;
import com.recruit.utils.BeanTransform;
import com.recruit.utils.ObjectTools;
import com.recruit.utils.SensitivewordFilter;
import com.recruit.utils.UserContext;
import com.recruit.vo.JobInformationVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Transactional
public class JobInformationService extends BaseServices<JobInformation,Long> {

    protected JobInformationRepository repository;

    public JobInformationService(JobInformationRepository repository) {
        super(repository);
        this.repository=repository;
    }

    @Autowired
    private WorkExperienceService workExperienceService;

    @Autowired
    private EducationExperienceService educationExperienceService;

    @Autowired
    private QualificationCertificateService qualificationCertificateService;

    @Autowired
    private UserService userService;


    public boolean saveOrUpdate(String json){

        SensitivewordFilter sensitivewordFilter = new SensitivewordFilter();
        JsonParser jsonParser = JsonParserFactory.getJsonParser();
        Map<String, Object> map = jsonParser.parseMap(json);

        JobInformation jobInformation = BeanTransform.map2Entity(map,JobInformation.class);

        Map<String, Object> getUserInfo = UserContext.getUser();
        if(null != getUserInfo){
            jobInformation.setUserId(ObjectTools.toLong(getUserInfo.get("id")));
        }

        if(null != jobInformation.getWorkExperience() && sensitivewordFilter.getSensitiveWord(jobInformation.getWorkExperience(),1)){
            return false;
        }

        if(null != jobInformation.getSelfAssessment() && sensitivewordFilter.getSensitiveWord(jobInformation.getSelfAssessment(),1)){
            return false;
        }

        this.saveOrUpdate(jobInformation);


        if(jobInformation.getReleaseType() == 2) {
            Set<Long> longs = new HashSet<>();
            Set<Long> longs1 = new HashSet<>();
            Set<Long> longs2 = new HashSet<>();
            //工作经历
            if (!ObjectTools.isNull(map.get("workLists"))) {
                List<Map<String, Object>> columns = (List<Map<String, Object>>) map.get("workLists");

                for(Map<String, Object> v : columns){
                    WorkExperience workExperience = BeanTransform.map2Entity(v,WorkExperience.class);
                    workExperience.setJobInformationId(jobInformation.getId());

                    if(sensitivewordFilter.getSensitiveWord(workExperience.getJobDescription(),1)){
                        return false;
                    }
                    workExperienceService.saveOrUpdate(workExperience);
                    longs.add(workExperience.getId());
                }

                //不存在的id就删除
                List<WorkExperience> workExperiences = workExperienceService.findByJobInformationId(ObjectTools.toLong(map.get("id")));
                workExperiences.stream().forEach(k -> {
                    if (!longs.contains(k.getId())) {
                        workExperienceService.delete(k.getId());
                    }
                });

            }


            //教育经历
            if (!ObjectTools.isNull(map.get("educationLists"))) {
                List<Map<String, Object>> columns = (List<Map<String, Object>>) map.get("educationLists");

                for(Map<String, Object> v : columns){
                    EducationExperience educationExperience = BeanTransform.map2Entity(v,EducationExperience.class);
                    educationExperience.setJobInformationId(jobInformation.getId());

                    if(sensitivewordFilter.getSensitiveWord(educationExperience.getProfessionalDescribe(),1)){
                        return false;
                    }
                    educationExperienceService.saveOrUpdate(educationExperience);
                    longs1.add(educationExperience.getId());
                }

                //不存在的id就删除
                List<EducationExperience> educationExperiences = educationExperienceService.findByJobInformationId(ObjectTools.toLong(map.get("id")));
                educationExperiences.stream().forEach(k -> {
                    if (!longs1.contains(k.getId())) {
                        educationExperienceService.delete(k.getId());
                    }
                });
            }

            //资格证书
            if (!ObjectTools.isNull(map.get("QCertificateLists"))) {
                List<Map<String, Object>> columns = (List<Map<String, Object>>) map.get("QCertificateLists");

                columns.stream().forEach(v -> {
                    QualificationCertificate qualificationCertificate = BeanTransform.map2Entity(v,QualificationCertificate.class);
                    qualificationCertificate.setJobInformationId(jobInformation.getId());
                    qualificationCertificateService.saveOrUpdate(qualificationCertificate);
                    longs2.add(qualificationCertificate.getId());
                });

                //不存在的id就删除
                List<QualificationCertificate> qualificationCertificates = qualificationCertificateService.findByJobInformationId(ObjectTools.toLong(map.get("id")));
                qualificationCertificates.stream().forEach(k -> {
                    if (!longs2.contains(k.getId())) {
                        qualificationCertificateService.delete(k.getId());
                    }
                });
            }
        }

        return true;
    }


    public Pager<JobInformationVo> pagerList(Pager<JobInformation> pager, JobInformationVo vo, Date startTime, Date endTime){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd-HH:mm:ss");

        String sql = "SELECT re1.*,re2.vx_number AS vx_number1,re2.phone AS phone1 FROM `re_job_information`  re1 \n" +
                     "LEFT JOIN `re_user` AS re2 ON re1.`user_id` = re2.id WHERE 1=1";

        if(!ObjectTools.isNull(startTime)) {
            sql += " and re1.create_date >=  '" + sdf.format(startTime) + "'  ";
        }

        if(!ObjectTools.isNull(endTime)) {
            sql += " and re1.create_date <=  '" + sdf.format(endTime) + "'  ";
        }

        if(!ObjectTools.isNull(vo.getUserPhone())) {
            sql += " and re2.phone = " + vo.getUserPhone() + "  ";
        }

        if(!ObjectTools.isNull(vo.getUserVxNumber())) {
            sql += " and re2.vx_number like '%" + vo.getUserVxNumber() + "%'  ";
        }

        if(!ObjectTools.isNull(vo.getResults())) {
            sql += " and re1.results = '" + vo.getResults() + "'  ";
        }

        if(!ObjectTools.isNull(vo.getReleaseType())) {
            sql += " and re1.release_type = " + vo.getReleaseType() + "  ";
        }

        StringBuffer hqlBuffer = new StringBuffer(sql);
        StringBuffer countHql = new StringBuffer(sql);

        this.appendHsql(pager, hqlBuffer, countHql, true);
        this.findPagerBySql(pager, hqlBuffer.toString()+ " order by re1.create_date desc ",
                "select count(1) from (" + hqlBuffer.toString() + ") b " ,false);

        Pager<JobInformationVo> pagerVo = pager.copy();
        List<JobInformationVo> voList = new Vector<>();
        List<JobInformation> list = pager.getContent();
        list.stream().forEach(k ->{
            JobInformationVo vo1 = findJobInformationVo(k);
            voList.add(vo1);
        });
        pagerVo.setContent(voList);

        return pagerVo;
    }

    public JobInformationVo findJobInformationVo(JobInformation jobInformation){

        JobInformationVo jobInformationVo = BeanTransform.copyProperties(jobInformation,JobInformationVo.class);
        jobInformationVo.setId(jobInformation.getId()+"");
        jobInformationVo.setCreateDate(jobInformation.getCreateDate());

        User user = userService.findOne(jobInformation.getUserId());

        if(null != user){
            jobInformationVo.setUserPhone(user.getPhone());
            jobInformationVo.setUserVxNumber(user.getVxNumber());
        }

        if(null != jobInformation.getContent() && !jobInformation.getContent().equals("")){
            jobInformationVo.setJob(jobInformation.getContent());
        }else {
            jobInformationVo.setJob(jobInformation.getJob());
        }

        if(StringUtils.isNotBlank(jobInformation.getStartSalary()) && StringUtils.isNotBlank(jobInformation.getEndSalary())){
            jobInformationVo.setSalary(jobInformation.getStartSalary() + "-" + jobInformation.getEndSalary());
        }else {
            jobInformationVo.setSalary("面议");
        }

        if(StringUtils.isNotBlank(jobInformation.getVillage())){
            jobInformationVo.setExpectPlace(jobInformation.getProvince()+jobInformation.getCity()+jobInformation.getArea()
                                                    +jobInformation.getVillage());
        }else {
            jobInformationVo.setExpectPlace(jobInformation.getProvince()+jobInformation.getArea()+jobInformation.getCity());
        }

        jobInformationVo.setNativeProvinceCity(jobInformation.getNativeProvince()+jobInformation.getNativeCity());

        if(jobInformation.getReleaseType() == 2){
            jobInformationVo.setIndustry(jobInformation.getIndustry());
            jobInformationVo.setFunctions(jobInformation.getFunctions());
            jobInformationVo.setEmail(jobInformation.getEmail());
            jobInformationVo.setSelfAssessment(jobInformation.getSelfAssessment());
        }

        return jobInformationVo;
    }


    public JobInformation details(Long id){

        JobInformation jobInformation = this.findOne(id);
        if(null != jobInformation){

            List<WorkExperience> workExperiences = workExperienceService.findByJobInformationId(jobInformation.getId());
            if(null != workExperiences){
                jobInformation.setWorkExperienceList(workExperiences);
            }

            List<EducationExperience> educationExperiences = educationExperienceService.findByJobInformationId(jobInformation.getId());
            if(null != educationExperiences){
                jobInformation.setEducationExperienceList(educationExperiences);
            }

            List<QualificationCertificate> qualificationCertificates = qualificationCertificateService.findByJobInformationId(jobInformation.getId());
            if(null != qualificationCertificates){
                jobInformation.setQualificationCertificateList(qualificationCertificates);
            }
        }

        return jobInformation;
    }


    public JobInformation updateResults(JobInformation jobInformation){

        JobInformation jobInformation1 = this.findOne(jobInformation.getId());

        if(null != jobInformation1 && jobInformation.getResults().equals("通过")){
            jobInformation1.setResults("通过");
            jobInformation1.setCause("");
            jobInformation1.setOtherContent("");
        }

        if(null != jobInformation1 && jobInformation.getResults().equals("不通过")){
            jobInformation1.setResults("不通过");
            jobInformation1.setCause(jobInformation.getCause());
            jobInformation1.setOtherContent(jobInformation.getOtherContent());
        }

        if(!jobInformation1.getCause().equals("其他") && !jobInformation1.getOtherContent().equals("") && null != jobInformation1.getOtherContent()){
            jobInformation1.setOtherContent("");
        }

        this.update(jobInformation1);

        return jobInformation1;
    }


    public Boolean batchResults(Long ids[]){

        Arrays.stream(ids).forEach(k ->{
            JobInformation jobInformation = this.findOne(k);
            if(null != jobInformation){
                jobInformation.setResults("通过");
                jobInformation.setCause("");
                jobInformation.setOtherContent("");
                this.update(jobInformation);
            }
        });
        return true;
    }

    public int findCountNum(){
        return repository.findCountNum();
    }

    public int findCountNumAndReleaseType(int type){
        return repository.findCountNumAndReleaseType(type);
    }


}
