package com.recruit.services;

import com.recruit.base.BaseServices;
import com.recruit.entity.InfoUploadFile;
import com.recruit.entity.PostArticle;
import com.recruit.entity.User;
import com.recruit.pager.Pager;
import com.recruit.repository.PostArticleRepository;
import com.recruit.utils.BeanTransform;
import com.recruit.utils.ObjectTools;
import com.recruit.utils.UploadUtils;
import com.recruit.utils.UserContext;
import com.recruit.vo.PostArticleVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class PostArticleService extends BaseServices<PostArticle,Long> {

    protected PostArticleRepository repository;

    public PostArticleService(PostArticleRepository repository) {
        super(repository);
        this.repository=repository;
    }

    @Autowired
    private InfoUploadFileService fileService;

    @Autowired
    private UserService userService;

    public PostArticle saveOrUpdate(PostArticle postArticle){

        PostArticle postArticle1 = null;

        Set<String> photoId = new HashSet<>();

        Map<String, Object> getUserInfo = UserContext.getUser();
        if(null != getUserInfo){
            postArticle.setUserId(ObjectTools.toLong(getUserInfo.get("id")));
        }

        if(postArticle.getId() != null && postArticle.getId() != 0){
            postArticle1 = this.findOne(postArticle.getId());
            photoId = new HashSet<>(StringUtils.isEmpty(postArticle1.getPhotoId()) ? new HashSet<>() : Arrays.asList(postArticle1.getPhotoId().split(",")));
        }else{
            super.saveOrUpdate(postArticle);
        }

        //企业图片
        if(postArticle.getPhotoIds() != null && postArticle.getPhotoIds().size() != 0){
            List<InfoUploadFile> list = editInfo(photoId, postArticle.getPhotoIds(),postArticle);
            postArticle.setPhotoIds(list);
            postArticle.setPhotoId(StringUtils.join(list.stream().map(InfoUploadFile::getId).collect(Collectors.toList()), ","));
        }else{
            postArticle.setPhotoIds(new ArrayList<>());
            postArticle.setPhotoId("");
        }
        super.saveOrUpdate(postArticle);

        //删除被删除的文件
        if(photoId.size() != 0){
            deleteFile(photoId);
        }

        return postArticle;
    }



    public  List<InfoUploadFile> editInfo (Set<String> set, List<InfoUploadFile> vo, PostArticle postArticle){
        List<InfoUploadFile> fileList = new ArrayList<>();
        for(int i = 0 ; i < vo.size() ; i++){
            InfoUploadFile restInfoUploadFileVo = vo.get(i);
            if(restInfoUploadFileVo.getId() != null && restInfoUploadFileVo.getId() != 0){
                set.remove(restInfoUploadFileVo.getId()+"");
                InfoUploadFile file = fileService.findOne(restInfoUploadFileVo.getId());
                if(file != null ){
                    //file.setVal(restInfoUploadFileVo.getVal());
                    restInfoUploadFileVo = fileService.saveOrUpdate(file);
                }
            }else{
                MultipartFile file = restInfoUploadFileVo.getFile();
                try {
                    restInfoUploadFileVo = fileService.upload2(UploadUtils.convertFile(file),restInfoUploadFileVo.getVal(),"贴子/",postArticle.getId());
                }catch (Exception e) {
                    System.out.println(e);
                    throw new RuntimeException("上传文件失败！");
                }
            }
            restInfoUploadFileVo.setFile(null);
            fileList.add(restInfoUploadFileVo);
        }
        //redisService.del("dirsTree");
        return fileList;
    }

    public void deleteFile( Set<String> set ){
        Long [] ids = new Long[set.size()];
        for(int i = 0 ; i < set.size() ; i++){
            ids[i] = Long.parseLong(set.toArray()[i]+"");
        }
        fileService.deletes(ids);
    }


    public Pager<PostArticleVo> pagerList(Pager<PostArticle> pager,PostArticleVo vo,Date startTime,Date endTime){


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd-HH:mm:ss");

        String sql = "SELECT re1.*,re2.vx_number AS vx_number1,re2.phone AS phone1 FROM `re_postarticle` re1\n" +
                     "LEFT JOIN `re_user` AS re2 ON re1.user_id = re2.id WHERE 1=1 ";

        if(!ObjectTools.isNull(startTime)) {
            sql += " and re1.create_date >=  '" + sdf.format(startTime) + "'  ";
        }

        if(!ObjectTools.isNull(endTime)) {
            sql += " and re1.create_date <=  '" + sdf.format(endTime) + "'  ";
        }

        if(!ObjectTools.isNull(vo.getUserPhone())) {
            sql += " and re2.phone = " + vo.getUserPhone() + "  ";
        }

        if(!ObjectTools.isNull(vo.getUserVxNumber())) {
            sql += " and re2.vx_number like '%" + vo.getUserVxNumber() + "%'  ";
        }

        if(!ObjectTools.isNull(vo.getResults())) {
            sql += " and re1.results = '" + vo.getResults() + "'  ";
        }

        StringBuffer hqlBuffer = new StringBuffer(sql);
        StringBuffer countHql = new StringBuffer(sql);

        this.appendHsql(pager, hqlBuffer, countHql, true);
        this.findPagerBySql(pager, hqlBuffer.toString()+ " order by re1.create_date desc ",
                "select count(1) from (" + hqlBuffer.toString() + ") b " ,false);

        Pager<PostArticleVo> pagerVo = pager.copy();
        List<PostArticleVo> voList = new Vector<>();
        List<PostArticle> list = pager.getContent();
        list.stream().forEach(k ->{
            PostArticleVo vo1 = findpostArticleVo(k);
            voList.add(vo1);
        });
        pagerVo.setContent(voList);

        return pagerVo;

    }

    public PostArticleVo findpostArticleVo(PostArticle postArticle){

        PostArticleVo vo = BeanTransform.copyProperties(postArticle,PostArticleVo.class);
        vo.setId(postArticle.getId() + "");
        vo.setCreateDate(postArticle.getCreateDate());

        User user = userService.findOne(postArticle.getUserId());
        if(null != user){
            vo.setUserPhone(user.getPhone());
            vo.setUserVxNumber(user.getVxNumber());
        }
        return vo;
    }


    public PostArticle updateResults(PostArticle postArticle){

        PostArticle postArticle1 = this.findOne(postArticle.getId());

        if(null != postArticle1 && postArticle.getResults().equals("通过")){
            postArticle1.setResults("通过");
            postArticle1.setCause("");
            postArticle1.setOtherContent("");
        }

        if(null != postArticle1 && postArticle.getResults().equals("不通过")){
            postArticle1.setResults("不通过");
            postArticle1.setCause(postArticle.getCause());
            postArticle1.setOtherContent(postArticle.getOtherContent());
        }

        if(!postArticle1.getCause().equals("其他") && !postArticle1.getOtherContent().equals("") && null != postArticle1.getOtherContent()){
            postArticle1.setOtherContent("");
        }

        this.update(postArticle1);

        return postArticle1;
    }


    public Boolean batchResults(Long ids[]){

        Arrays.stream(ids).forEach(k ->{
            PostArticle postArticle = this.findOne(k);
            if(null != postArticle){
                postArticle.setResults("通过");
                postArticle.setCause("");
                postArticle.setOtherContent("");
                this.update(postArticle);
            }
        });
        return true;
    }


    public int findCountNum(){
        return repository.findCountNum();
    }


}
