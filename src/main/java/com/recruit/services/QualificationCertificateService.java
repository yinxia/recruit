package com.recruit.services;

import com.recruit.base.BaseServices;
import com.recruit.entity.QualificationCertificate;
import com.recruit.repository.QualificationCertificateRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class QualificationCertificateService extends BaseServices<QualificationCertificate,Long> {

    protected QualificationCertificateRepository repository;

    public QualificationCertificateService(QualificationCertificateRepository repository) {
        super(repository);
        this.repository=repository;
    }

    public List<QualificationCertificate> findByJobInformationId(Long jobInformationId){
        return repository.findByJobInformationId(jobInformationId);
    }

}
