package com.recruit.services;

import com.recruit.base.BaseServices;
import com.recruit.entity.Display;
import com.recruit.repository.DisplayRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class DisplayService extends BaseServices<Display,Long> {

    protected DisplayRepository repository;

    public DisplayService(DisplayRepository repository) {
        super(repository);
        this.repository=repository;
    }

}
