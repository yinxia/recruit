package com.recruit.services;

import com.recruit.base.BaseServices;
import com.recruit.entity.FactoryRecruitmentType;
import com.recruit.repository.FactoryRecruitmentTypeRepositoey;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class FactoryRecruitmentTypeService extends BaseServices<FactoryRecruitmentType,Long> {

    protected FactoryRecruitmentTypeRepositoey repository;

    public FactoryRecruitmentTypeService(FactoryRecruitmentTypeRepositoey repository) {
        super(repository);
        this.repository=repository;
    }

    public List<FactoryRecruitmentType> findByFactoryRecruitmentId(Long factoryRecruitmentTypeId){
        return repository.findByFactoryRecruitmentId(factoryRecruitmentTypeId);
    }
}
