package com.recruit.services;


import com.recruit.base.BaseServices;
import com.recruit.entity.InfoUploadFile;
import com.recruit.entity.Releases;
import com.recruit.entity.User;
import com.recruit.pager.Pager;
import com.recruit.repository.ReleasesRepository;
import com.recruit.utils.BeanTransform;
import com.recruit.utils.ObjectTools;
import com.recruit.utils.UploadUtils;
import com.recruit.utils.UserContext;
import com.recruit.vo.ReleasesVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class ReleasesService extends BaseServices<Releases,Long> {

    protected ReleasesRepository repository;

    public ReleasesService(ReleasesRepository repository) {
        super(repository);
        this.repository=repository;
    }

    @Autowired
    private InfoUploadFileService fileService;

    @Autowired
    private UserService userService;


    public Releases saveOrUpdate(Releases releases){

        Releases releases1 = null;

        Set<String> companiesPhotoIds = new HashSet<>();

        Map<String, Object> getUserInfo = UserContext.getUser();
        if(null != getUserInfo){
            releases.setUserId(ObjectTools.toLong(getUserInfo.get("id")));
        }

        if(releases.getId() != null && releases.getId() != 0){
            releases1 = this.findOne(releases.getId());
            companiesPhotoIds = new HashSet<>(StringUtils.isEmpty(releases1.getCompaniesPhotoId()) ? new HashSet<>() : Arrays.asList(releases1.getCompaniesPhotoId().split(",")));
        }else{
            super.saveOrUpdate(releases);
        }

        //企业图片
        if(releases.getCompaniesPhotoIds() != null && releases.getCompaniesPhotoIds().size() != 0){
            List<InfoUploadFile> list = editInfo(companiesPhotoIds, releases.getCompaniesPhotoIds(),typeName(releases.getTypeList()),releases,releases.getEnterpriseName());
            releases.setCompaniesPhotoIds(list);
            releases.setCompaniesPhotoId(StringUtils.join(list.stream().map(InfoUploadFile::getId).collect(Collectors.toList()), ","));
        }else{
            releases.setCompaniesPhotoIds(new ArrayList<>());
            releases.setCompaniesPhotoId("");
        }
        super.saveOrUpdate(releases);

        //删除被删除的文件
        if(companiesPhotoIds.size() != 0){
            deleteFile(companiesPhotoIds);
        }

        return releases;
    }


    public String typeName(int typeList){

        String str = null;

        switch (typeList){
            case 1:
                str = "合作加工发布";
               break;
            case 2:
                str = "二手转让类别";
                break;
            case 3:
                str = "绣印洗水";
                break;
            case 4:
                str = "布行辅料";
                break;
            default:
                str = "";
                break;
        }

        return str;
    }


    public void deleteFile( Set<String> set ){
        Long [] ids = new Long[set.size()];
        for(int i = 0 ; i < set.size() ; i++){
            ids[i] = Long.parseLong(set.toArray()[i]+"");
        }
        fileService.deletes(ids);
    }



    public List<InfoUploadFile> editInfo (Set<String> set, List<InfoUploadFile> vo, String name ,Releases releases, String title){
        List<InfoUploadFile> fileList = new ArrayList<>();
        for(int i = 0 ; i < vo.size() ; i++){
            InfoUploadFile restInfoUploadFileVo = vo.get(i);
            if(restInfoUploadFileVo.getId() != null && restInfoUploadFileVo.getId() != 0){
                set.remove(restInfoUploadFileVo.getId()+"");
                InfoUploadFile file = fileService.findOne(restInfoUploadFileVo.getId());
                if(file != null ){
                    //file.setVal(restInfoUploadFileVo.getVal());
                    restInfoUploadFileVo = fileService.saveOrUpdate(file);
                }
            }else{
                MultipartFile file = restInfoUploadFileVo.getFile();
                try {
                    restInfoUploadFileVo = fileService.upload2(UploadUtils.convertFile(file),restInfoUploadFileVo.getVal(),name+"/"+title,releases.getId());
                }catch (Exception e) {
                    System.out.println(e);
                    throw new RuntimeException("上传文件失败！");
                }
            }
            restInfoUploadFileVo.setFile(null);
            fileList.add(restInfoUploadFileVo);
        }
        //redisService.del("dirsTree");
        return fileList;
    }


    public Pager<ReleasesVo> pagerList(Pager<Releases> pager,ReleasesVo vo,Date startTime,Date endTime){


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd-HH:mm:ss");

        String sql = "SELECT re1.*,re2.vx_number AS vx_number1,re2.phone AS phone1 FROM `re_releases` re1\n" +
                        "LEFT JOIN `re_user` AS re2 ON re1.`user_id` = re2.id WHERE 1=1 " ;

        if(!ObjectTools.isNull(startTime)) {
            sql += " and re1.create_date >=  '" + sdf.format(startTime) + "'  ";
        }

        if(!ObjectTools.isNull(endTime)) {
            sql += " and re1.create_date <=  '" + sdf.format(endTime) + "'  ";
        }

        if(!ObjectTools.isNull(vo.getUserPhone())) {
            sql += " and re2.phone = " + vo.getUserPhone() + "  ";
        }

        if(!ObjectTools.isNull(vo.getUserVxNumber())) {
            sql += " and re2.vx_number like '%" + vo.getUserVxNumber() + "%'  ";
        }

        if(!ObjectTools.isNull(vo.getResults())) {
            sql += " and re1.results = '" + vo.getResults() + "'  ";
        }

        if(!ObjectTools.isNull(vo.getTypeList())) {
            sql += " and re1.type_list = " + vo.getTypeList() + "  ";
        }

        if(!ObjectTools.isNull(vo.getSecondTransferType())) {
            sql += " and re1.second_transfer_type = " + vo.getSecondTransferType() + "  ";
        }

        StringBuffer hqlBuffer = new StringBuffer(sql);
        StringBuffer countHql = new StringBuffer(sql);

        this.appendHsql(pager, hqlBuffer, countHql, true);
        this.findPagerBySql(pager, hqlBuffer.toString()+ " order by re1.create_date desc ",
                "select count(1) from (" + hqlBuffer.toString() + ") b " ,false);

        Pager<ReleasesVo> pagerVo = pager.copy();
        List<ReleasesVo> voList = new Vector<>();
        List<Releases> list = pager.getContent();
        list.stream().forEach(k ->{
            ReleasesVo vo1 = findReleasesVo(k);
            voList.add(vo1);
        });
        pagerVo.setContent(voList);

        return pagerVo;
    }



    public ReleasesVo findReleasesVo(Releases releases){

        ReleasesVo vo = BeanTransform.copyProperties(releases,ReleasesVo.class);
        vo.setId(releases.getId()+"");
        if(null != releases.getOther() && !releases.getOther().equals("")){
            vo.setCategory(releases.getOther());
        }else {
            vo.setCategory(releases.getCategory());
        }

        if(releases.getSecondTransferType() == 1 && null == releases.getMoney()  && !releases.getMoney().equals("")){
            vo.setMoney("面议");
        }
        vo.setCreateDate(releases.getCreateDate());
        User user = userService.findOne(releases.getUserId());

        if(null != user){
            vo.setUserPhone(user.getPhone());
            vo.setUserVxNumber(user.getVxNumber());
        }

        return vo;
    }


    public Releases updateResults(Releases releases){

        Releases releases1 = this.findOne(releases.getId());

        if(null != releases1 && releases.getResults().equals("通过")){
            releases1.setResults("通过");
            releases1.setCause("");
            releases1.setOtherContent("");
        }

        if(null != releases1 && releases.getResults().equals("不通过")){
            releases1.setResults("不通过");
            releases1.setCause(releases.getCause());
            releases1.setOtherContent(releases.getOtherContent());
        }

        if(!releases1.getCause().equals("其他") && !releases1.getOtherContent().equals("") && null != releases1.getOtherContent()){
            releases1.setOtherContent("");
        }

        this.update(releases1);

        return releases1;
    }


    public Boolean batchResults(Long ids[]){

        Arrays.stream(ids).forEach(k ->{
            Releases releases = this.findOne(k);
            if(null != releases){
                releases.setResults("通过");
                releases.setCause("");
                releases.setOtherContent("");
                this.update(releases);
            }
        });
        return true;
    }

    public int findCountNum(int typeList){
        return repository.findCountNum(typeList);
    }

    public int findCountNumAndType(int typeList,int secondTransferType){
        return repository.findCountNumAndType(typeList,secondTransferType);
    }




}
