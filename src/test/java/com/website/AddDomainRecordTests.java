package com.website;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.alidns.model.v20150109.AddDomainRecordRequest;
import com.aliyuncs.alidns.model.v20150109.AddDomainRecordResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.google.gson.Gson;


/**
 * @ClassName: AddDomainRecordTests
 * @Author: 陈天频
 * @Date: 19-7-18 下午5:12
 * @Desription:
 * @Copy: com.bjike
 **/
public class AddDomainRecordTests {

   /* public static void main(String[] args) {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "<accessKeyId>", "<accessSecret>");
        IAcsClient client = new DefaultAcsClient(profile);

        AddDomainRecordRequest request = new AddDomainRecordRequest();
        request.setType("A");
        request.setRR("bjike.com");
        request.setDomainName("tkgw");

        try {
            AddDomainRecordResponse response = client.getAcsResponse(request);
            System.out.println(new Gson().toJson(response));
        } catch (ClientException e) {
            System.out.println("ErrCode:" + e.getErrCode());
            System.out.println("ErrMsg:" + e.getErrMsg());
            System.out.println("RequestId:" + e.getRequestId());
        }

    }*/

    public static void main(String[] args) {
        // 创建DefaultAcsClient实例并初始化
        DefaultProfile profile = DefaultProfile.getProfile(
                "cn-hangzhou",          // 地域ID
                "LTAIIiJILKkH0JGY",      // RAM账号的AccessKey ID
                "KmmxKgS8f3MVIdAEQ3NTv2Qzouetu1"); // RAM账号AccessKey Secret
        IAcsClient client = new DefaultAcsClient(profile);
        // 创建API请求并设置参数
        AddDomainRecordRequest request = new AddDomainRecordRequest();
        request.setValue("39.77.106.163");
        request.setType("A");
        request.setRR("jhtest");
        request.setDomainName("bjike.com");
        // 发起请求并处理应答或异常
        try {
            AddDomainRecordResponse response = client.getAcsResponse(request);
            System.out.println(new Gson().toJson(response));
        } catch (ClientException e) {
            System.out.println("ErrCode:" + e.getErrCode());
            System.out.println("ErrMsg:" + e.getErrMsg());
            System.out.println("RequestId:" + e.getRequestId());
        }

    }
}
