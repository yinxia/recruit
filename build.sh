#!/bin/bash

profile=$1
echo profile:$profile

isPro=`echo $profile | grep pro`

echo $isPro

if [ -n "$isPro" ] ; then
    echo "copy etc/application.yml"
    /usr/bin/cp etc/application.yml src/main/resources/ ;
fi

mvn package -DskipTests -Djar

docker rm -f recruit

docker rmi -f recruit

docker build -t recruit -f Dockerfile .

if [ -d "/data/recruit/" ];then
	echo "/data/recruit/"
else
	mkdir -p /data/recruit/

chmod 777 /data/recruit
fi


docker run -d -p 8093:8093  -v /data/recruit:/data/recruit --name recruit recruit
